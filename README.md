# OSS (Overseas student service) #

OSS helps overseas students to find Accomodation, Career support and Consultancy


### Featurs ###

* Career Support
* Accomodation
* Consultancy
* Chat with active users
* Feeds


### Screenshots ###
![splash](/screenshots/splash.jpeg)
![navigation](/screenshots/navigation.jpeg)
![feed](/screenshots/feed.jpeg)
![services](/screenshots/services.jpeg)
![chat](/screenshots/chat.jpeg)