package com.oss.oss.Login;


import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.oss.oss.R;
import com.oss.oss.Rest.DataManager;
import com.oss.oss.Rest.model.ModelChats;
import com.oss.oss.Rest.model.ModelUser;
import com.oss.oss.Rest.network.RemoteCallback;
import com.oss.oss.home.HomeActivity;
import com.oss.oss.utils.TagsAndConstants;
import com.oss.oss.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import timber.log.Timber;

/**
 * RegisterFragment - Register fragment
 *
 * @author Sapun Wiratunga <s.wiratunga@cqumail.com>
 * @version 1.0
 * @since 1.0
 */
public class RegisterFragment extends Fragment {


    public static final String TAG = RegisterFragment.class.getSimpleName();
    @BindView(R.id.txtRegDob)
    TextView txtRegDob;
    Unbinder unbinder;
    @BindView(R.id.txtRegVGD)
    TextView txtRegVGD;
    @BindView(R.id.txtRegVED)
    TextView txtRegVED;
    @BindView(R.id.btnRegGenderM)
    Button btnRegGenderM;
    @BindView(R.id.btnRegGenderF)
    Button btnRegGenderF;
    @BindView(R.id.btnReg)
    Button btnReg;
    @BindView(R.id.titleLogin)
    TextView titleLogin;
    @BindView(R.id.edtRegFname)
    EditText edtRegFname;
    @BindView(R.id.edtRegLname)
    EditText edtRegLname;
    @BindView(R.id.edtRegEmail)
    EditText edtRegEmail;
    @BindView(R.id.edtRegPass)
    EditText edtRegPass;
    @BindView(R.id.edtRegAddress)
    EditText edtRegAddress;
    @BindView(R.id.edtRegUniversity)
    EditText edtRegUniversity;
    @BindView(R.id.txtRegDobError)
    TextView txtRegDobError;
    @BindView(R.id.txtRegVgdError)
    TextView txtRegVgdError;
    @BindView(R.id.txtRegVedError)
    TextView txtRegVedError;
    private String fname;
    private String lname;
    private String gender;
    private String dob;
    private String email;
    private String password;
    private String address;
    private String university;
    private String visaGrantDate;
    private String visaExpiryDate;

    public RegisterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_register, container, false);
        unbinder = ButterKnife.bind(this, view);

        //select male by default
        btnRegGenderM.setSelected(true);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.txtRegVED, R.id.txtRegDob, R.id.txtRegVGD, R.id.btnRegGenderM, R.id.btnRegGenderF, R.id.btnReg})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtRegVED:
                //pick visa expiry date
                pickVisaExpiryDate();
                break;
            case R.id.txtRegVGD:
                //pick visa grant date
                pickVisaGrantDate();
                break;
            case R.id.txtRegDob:
                //pick date of birth
                pickDOB();
                break;
            case R.id.btnRegGenderM:
                //Select gender as male
                selectGender(true);
                break;
            case R.id.btnRegGenderF:
                //Select gender as female
                selectGender(false);
                break;
            case R.id.btnReg:
                if (validation()) {
                    register();
                }
                break;

        }
    }

    /**
     * Select gender
     *
     * @param isMale is selection is male
     */
    private void selectGender(boolean isMale) {
        btnRegGenderM.setSelected(isMale ? true : false);
        btnRegGenderF.setSelected(isMale ? false : true);
    }

    /**
     * Pick Date of Birth from date picker dialog
     */
    private void pickDOB() {
        DatePickerDialog dobDialog = new DatePickerDialog(
                getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                txtRegDob.setText(String.format("%d-%d-%d", year, month + 1, dayOfMonth));
            }
        }, 2010, 01, 01);
        dobDialog.show();
    }

    /**
     * Pick Visa grant date from date picker dialog
     */
    private void pickVisaGrantDate() {
        DatePickerDialog visaGrantDialog = new DatePickerDialog(
                getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                txtRegVGD.setText(String.format("%d-%d-%d", year, month + 1, dayOfMonth));
            }
        }, 2010, 01, 01);
        visaGrantDialog.show();
    }

    /**
     * Pick Visa expiry date from date picker dialog
     */
    private void pickVisaExpiryDate() {
        DatePickerDialog visaExpiryDialog = new DatePickerDialog(
                getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                txtRegVED.setText(String.format("%d-%d-%d", year, month + 1, dayOfMonth));
            }
        }, 2010, 01, 01);
        visaExpiryDialog.show();
    }

    /**
     * Register new user
     *
     * @see com.oss.oss.Rest.network.RemoteServices#register(String, String, String, String, String, String, String, String, String, String)
     */
    private void register() {

        Utils.showProgressDialog(getActivity());
        DataManager.getInstance().register(fname, lname, gender, dob, email, password, address, university, visaGrantDate, visaExpiryDate, new RemoteCallback<ModelUser>() {
            @Override
            public void onSuccess(ModelUser response) {
                Utils.hideProgressDialog();
                Timber.i("Register" + new Gson().toJson(response));
                Toast.makeText(getActivity(), "" + response.getMsg(), Toast.LENGTH_SHORT).show();
                if (!response.isError()) {

                    //update user in preference
                    Utils.getSharedPreferenceEditor(getActivity()).putString(TagsAndConstants.TAG_USER_JSON, new Gson().toJson(response)).commit();
                    //update user singleton
                    ModelUser.setInstance(response);

                    startActivity(new Intent(getActivity(), HomeActivity.class));
                    getActivity().finish();

                }
            }

            @Override
            public void onFailed(Throwable throwable) {
                Utils.hideProgressDialog();
                Timber.e(throwable, "Register");
            }
        });

    }


    /**
     * Validate all input fields
     *
     * @return validation status
     */
    private boolean validation() {
        boolean validation = true;
        //reset error text fields
        txtRegDobError.setVisibility(View.GONE);
        txtRegVedError.setVisibility(View.GONE);
        txtRegVgdError.setVisibility(View.GONE);

        fname = edtRegFname.getText().toString().trim();
        lname = edtRegLname.getText().toString().trim();
        dob = txtRegDob.getText().toString().trim();
        gender = (btnRegGenderM.isSelected()) ? "male" : "female";
        Toast.makeText(getActivity(), "gender: "+gender, Toast.LENGTH_SHORT).show();
        email = edtRegEmail.getText().toString().trim();
        password = edtRegPass.getText().toString().trim();
        address = edtRegAddress.getText().toString().trim();
        university = edtRegUniversity.getText().toString().trim();
        visaGrantDate = txtRegVGD.getText().toString().trim();
        visaExpiryDate = txtRegVED.getText().toString().trim();


        if (TextUtils.isEmpty(visaGrantDate)) {
            txtRegVgdError.setText(getResources().getString(R.string.error_vgd));
            txtRegVgdError.setVisibility(View.VISIBLE);
            validation = false;
        }
        if (TextUtils.isEmpty(visaExpiryDate)) {
            txtRegVedError.setText(getResources().getString(R.string.error_ved));
            txtRegVedError.setVisibility(View.VISIBLE);
            validation = false;
        }
        if (TextUtils.isEmpty(university)) {
            edtRegUniversity.setError(getResources().getString(R.string.error_university));
            edtRegUniversity.requestFocus();
            validation = false;
        }
        if (TextUtils.isEmpty(address)) {
            edtRegAddress.setError(getResources().getString(R.string.error_address));
            edtRegAddress.requestFocus();
            validation = false;
        }
        if (TextUtils.isEmpty(password)) {
            edtRegPass.setError(getResources().getString(R.string.error_pass));
            edtRegPass.requestFocus();
            validation = false;
        }
        if (TextUtils.isEmpty(email) || !(Patterns.EMAIL_ADDRESS.matcher(email).matches())) {
            edtRegEmail.setError(getResources().getString(R.string.error_email));
            edtRegEmail.requestFocus();
            validation = false;
        }
        if (TextUtils.isEmpty(dob)) {
            txtRegDobError.setText(getResources().getString(R.string.error_dob));
            txtRegDobError.setVisibility(View.VISIBLE);
            validation = false;
        }
        if (TextUtils.isEmpty(lname)) {
            edtRegLname.setError(getResources().getString(R.string.error_lname));
            edtRegFname.requestFocus();
            validation = false;
        }
        if (TextUtils.isEmpty(fname)) {
            edtRegFname.setError(getResources().getString(R.string.error_fname));
            edtRegFname.requestFocus();
            validation = false;
        }
        return validation;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
