package com.oss.oss.Login;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.oss.oss.R;
import com.oss.oss.utils.FragmentManagerUtil;

/**
 * UserActivity - User activity which holds {@link LoginFragment} and {@link RegisterFragment}
 * @author Sapun Wiratunga <s.wiratunga@cqumail.com>
 * @version 1.0
 * @since 1.0
 */
public class UserActivity extends AppCompatActivity implements View.OnClickListener {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Button btnUserLogin = (Button) findViewById(R.id.btnUserLogin);
        Button btnUserSignup = (Button) findViewById(R.id.btnUserSignup);

        btnUserLogin.setOnClickListener(this);
        btnUserSignup.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnUserLogin:
                //show loin fragment
                FragmentManagerUtil.replaceFragment(getSupportFragmentManager(),R.id.frameUser,new LoginFragment(),true,LoginFragment.TAG);
                break;
            case R.id.btnUserSignup:
                //show register fragment
                FragmentManagerUtil.replaceFragment(getSupportFragmentManager(),R.id.frameUser,new RegisterFragment(),true,RegisterFragment.TAG);
                break;
        }
    }
}
