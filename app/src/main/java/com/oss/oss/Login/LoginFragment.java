package com.oss.oss.Login;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.oss.oss.R;
import com.oss.oss.Rest.DataManager;
import com.oss.oss.Rest.model.ModelChats;
import com.oss.oss.Rest.model.ModelUser;
import com.oss.oss.Rest.network.RemoteCallback;
import com.oss.oss.admin.AdminActivity;
import com.oss.oss.home.HomeActivity;
import com.oss.oss.utils.TagsAndConstants;
import com.oss.oss.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import timber.log.Timber;

/**
 * LoginFragment - Login fragment
 *
 * @author Sapun Wiratunga <s.wiratunga@cqumail.com>
 * @version 1.0
 * @since 1.0
 */
public class LoginFragment extends Fragment {


    public static final String TAG = LoginFragment.class.getSimpleName();
    @BindView(R.id.edtLoginEmail)
    EditText edtLoginEmail;
    @BindView(R.id.edtLoginPass)
    EditText edtLoginPass;
    @BindView(R.id.btnLogin)
    Button btnLogin;
    Unbinder unbinder;
    private String email;
    private String pass;

    /**
     * Constructor
     */
    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.btnLogin)
    public void onViewClicked() {
        if (validation()) {
            login();
        }
    }

    /**
     * Login
     *
     * @see com.oss.oss.Rest.network.RemoteServices#login(String, String)
     */
    private void login() {
        Utils.showProgressDialog(getActivity());
        DataManager.getInstance().login(email, pass, new RemoteCallback<ModelUser>() {
            @Override
            public void onSuccess(ModelUser response) {
                Timber.i(new Gson().toJson(response));
                Toast.makeText(getActivity(), "" + response.getMsg(), Toast.LENGTH_SHORT).show();
                if (!response.isError()) {//if login successful

                    //update user in preference
                    Utils.getSharedPreferenceEditor(getActivity()).putString(TagsAndConstants.TAG_USER_JSON, new Gson().toJson(response)).commit();
                    //update user singleton
                    ModelUser.setInstance(response);

                    String user_type = response.getUser().getUser_type();
                    downloadUsersChat(user_type);

                }else{
                    Utils.hideProgressDialog();
                }
            }

            @Override
            public void onFailed(Throwable throwable) {
                Timber.e(throwable, "login");
                Utils.hideProgressDialog();
            }
        });
    }

    private void downloadUsersChat(final String user_type) {
        DataManager.getInstance().getChats(ModelUser.getInstance().getUser().getUser_id(), new RemoteCallback<ModelChats>() {
            @Override
            public void onSuccess(ModelChats response) {
                Timber.i(new Gson().toJson(response));
                Utils.hideProgressDialog();
                if(!response.isError()){
                    ModelChats.setInstance(response);
                    Utils.getSharedPreferenceEditor(getActivity()).putString(TagsAndConstants.TAG_CHATS_JSON,new Gson().toJson(response)).commit();
                    navigateUser(user_type);
                }else{
                    //reset user
                    ModelUser.setInstance("{}");
                    Toast.makeText(getActivity(), "Can't download users data please try again", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailed(Throwable throwable) {
                Timber.e(throwable, "downloadUsersChat");
                Utils.hideProgressDialog();
            }
        });
    }

    private void navigateUser(String user_type) {
        if (user_type.equalsIgnoreCase(TagsAndConstants.USER_TYPE_ADMIN)) {
            startActivity(new Intent(getActivity(), AdminActivity.class));
            getActivity().finish();
        } else {
            startActivity(new Intent(getActivity(), HomeActivity.class));
            getActivity().finish();
        }
    }

    /**
     * Input fields validation
     *
     * @return validation status
     */
    private boolean validation() {
        boolean validation = true;

        email = edtLoginEmail.getText().toString().trim();
        pass = edtLoginPass.getText().toString().trim();

        if (TextUtils.isEmpty(email)) {
            validation = false;
            edtLoginEmail.setError(getResources().getString(R.string.error_username));
        }
        if (TextUtils.isEmpty(email)) {
            validation = false;
            edtLoginPass.setError(getResources().getString(R.string.error_pass));
        }
        return validation;
    }
}
