package com.oss.oss.chat;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oss.oss.R;
import com.oss.oss.Rest.Resources.ResMsg;
import com.oss.oss.Rest.model.ModelUser;
import com.oss.oss.utils.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * ChatActivityAdapter - Adapter for chat Activity messages list
 *
 * @author Sapun Wiratunga <s.wiratunga@cqumail.com>
 * @version 1.0
 * @since 1.0
 */
public class ChatActivityAdapter extends RecyclerView.Adapter<ChatActivityAdapter.ViewHolder> {

    private final List<ResMsg> mValues;
    private String chatRecipientName;
    private Context context;
    final int VIEW_TYPE_CHAT_UP=0;
    final int VIEW_TYPE_CHAT_DOWN=1;

    /**
     * Constructor
     *  @param ctx   Context from where it gets invoked
     * @param items list of Posts
     * @param chatRecipientName
     */
    public ChatActivityAdapter(Context ctx, List<ResMsg> items, String chatRecipientName) {
        context = ctx;
        mValues = items;
        this.chatRecipientName = chatRecipientName;
    }

    @Override
    public int getItemViewType(int position) {

        return (mValues.get(position).getSent_by().equalsIgnoreCase(ModelUser.getInstance().getUser().getUser_id()))?VIEW_TYPE_CHAT_UP:VIEW_TYPE_CHAT_DOWN;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        if(viewType== VIEW_TYPE_CHAT_UP){
            return new ViewHolder(inflater.inflate(R.layout.item_message_frag_up,parent,false));
        }else{
            return new ViewHolder(inflater.inflate(R.layout.item_message_frag_down,parent,false));
        }

    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        //bind post item with viewholder
        ((ViewHolder) holder).bindData();

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    /**
     * View holder for post items extends {@link ViewHolder}
     */
    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public ResMsg mItem;
        @BindView(R.id.txtItemSender)
        TextView txtItemSender;
        @BindView(R.id.txtItemMessage)
        TextView txtItemMessage;
        @BindView(R.id.txtItemTime)
        TextView txtItemTime;

        public ViewHolder(View view) {
            super(view);
            mView = view;

            //Butterknif injection
            ButterKnife.bind(this, view);

        }

        public void bindData() {

            String username = (mItem.getSent_by().equalsIgnoreCase(ModelUser.getInstance().getUser().getUser_id())) ? ModelUser.getInstance().getUser().getF_name() : chatRecipientName;
            txtItemSender.setText(username);
            txtItemMessage.setText(mItem.getMsg_text());
            String relativeTime = Utils.calculateTimeAgo(Utils.getDateInMillis(mItem.getSent_time()), System.currentTimeMillis());
            txtItemTime.setText(String.format("%s", relativeTime));

        }
    }
}
