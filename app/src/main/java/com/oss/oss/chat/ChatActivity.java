package com.oss.oss.chat;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oss.oss.R;
import com.oss.oss.Rest.DataManager;
import com.oss.oss.Rest.Resources.ResMsg;
import com.oss.oss.Rest.model.ModelAllMsg;
import com.oss.oss.Rest.model.ModelNewMsg;
import com.oss.oss.Rest.model.ModelUser;
import com.oss.oss.Rest.network.RemoteCallback;
import com.oss.oss.utils.TagsAndConstants;
import com.oss.oss.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;

public class ChatActivity extends AppCompatActivity {

    @BindView(R.id.txtDataStatus)
    TextView txtDataStatus;
    @BindView(R.id.list)
    RecyclerView list;
    @BindView(R.id.edtChat)
    EditText edtChat;
    @BindView(R.id.imgChatSend)
    ImageView imgChatSend;
    private List<ResMsg> msgsList = new ArrayList<>();
    private ChatActivityAdapter adapter;
    private String chatRecipientUserId = "";
    private String chatRecipientName = "";
    private String chatRecipientEmail = "";
    private boolean isSendingMsg = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (getIntent().getExtras() != null) {
            chatRecipientName = getIntent().getExtras().getString(TagsAndConstants.EXTRA_CHAT_RECIEVER_NAME);
            chatRecipientEmail = getIntent().getExtras().getString(TagsAndConstants.EXTRA_CHAT_RECIEVER_EMAIL);
            chatRecipientUserId = getIntent().getExtras().getString(TagsAndConstants.EXTRA_CHAT_RECIEVER_USER_ID);
            getSupportActionBar().setTitle(chatRecipientName);
            getSupportActionBar().setSubtitle(chatRecipientEmail);
        }

        setupList();

        downloadMessages();
    }

    private void setupList() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setReverseLayout(true);
        list.setLayoutManager(linearLayoutManager);
        adapter = new ChatActivityAdapter(this, msgsList, chatRecipientName);
        list.setAdapter(adapter);

    }

    private void downloadMessages() {
        DataManager.getInstance().getMessagesWithUser(ModelUser.getInstance().getUser().getUser_id(), chatRecipientUserId, new RemoteCallback<ModelAllMsg>() {
            @Override
            public void onSuccess(ModelAllMsg response) {
                Timber.i("downloadMessages :" + new Gson().toJson(response));
                if (!response.isError()) {
                    list.setVisibility(View.VISIBLE);
                    txtDataStatus.setVisibility(View.GONE);

                    msgsList.clear();
                    msgsList.addAll(response.getMessage());
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailed(Throwable throwable) {
                Timber.e(throwable, "download messages");
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_refresh:
                downloadMessages();
                break;
            default:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_chat, menu);
        return true;
    }


    @OnClick(R.id.imgChatSend)
    public void onViewClicked() {

        if (!isSendingMsg) {
            String text = edtChat.getText().toString().trim();

            //do not send message if message text is empty
            if (TextUtils.isEmpty(text)) {
                return;
            }

            isSendingMsg = true;
            DataManager.getInstance().sendMsg(text, ModelUser.getInstance().getUser().getUser_id(), Utils.getCurrentDate(), chatRecipientUserId, new RemoteCallback<ModelNewMsg>() {
                @Override
                public void onSuccess(ModelNewMsg response) {
                    Timber.i("send msg: " + new Gson().toJson(response));
                    isSendingMsg = false;

                    if (!response.isError()) {
                        msgsList.add(0, response.getMessage());
                        adapter.notifyItemInserted(0);
                        edtChat.setText("");
                    }
                }

                @Override
                public void onFailed(Throwable throwable) {
                    Timber.e(throwable, "send msg:");
                    isSendingMsg = false;
                }
            });
        }


    }
}
