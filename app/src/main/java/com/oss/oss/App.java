package com.oss.oss;

import android.app.Application;

import timber.log.Timber;

/**
 * App - Custom application class
 *
 * @author Sapun Wiratunga <s.wiratunga@cqumail.com>
 * @version 1.0
 * @since 1.0
 */
public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        //Setup Timber logging system
        Timber.plant(new Timber.DebugTree() {
            @Override
            protected String createStackElementTag(StackTraceElement element) {
                return super.createStackElementTag(element) + ":" + element.getLineNumber() + ":" + element.getMethodName();
            }
        });
    }
}
