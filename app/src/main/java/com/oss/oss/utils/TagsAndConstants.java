package com.oss.oss.utils;

import android.content.Context;

/**
 * TagsAndConstants - utility class for store all Tags and constant values for application
 *
 * @author Sapun Wiratunga <s.wiratunga@cqumail.com>
 * @version 1.0
 * @since 1.0
 */
public class TagsAndConstants {

    public static final String LOCAL_SERVER = "http://10.0.2.2/oss_api";
    public static final String LIVE_SERVER = "http://ossproject.16mb.com";
    public static final String DOMAIN_NAME = LIVE_SERVER;
//    public static final String DOMAIN_NAME =   "http://192.168.43.121";

    //these fields should be same as in database
    public static final String USER_TYPE_ADMIN = "admin";
    public static final String PREFERENCE_NAME = "OSS_PREFERENCES";
    public static final String TAG_USER_JSON = "tag_user_json";

    public static final String SERVICE_TYPE_CONSULTANCY = "consultancy";
    public static final String SERVICE_TYPE_CAREER_SUPPORT = "career support";
    public static final String SERVICE_TYPE_ACCOMODATION = "accomodation";

    public static final String EXTRA_SERVICE_TYPE = "service_type";
    public static final String TAG_CHATS_JSON = "tag_chats_json";
    public static final String EXTRA_CHAT_RECIEVER_NAME = "extra_chat_reciever_name";
    public static final String EXTRA_CHAT_RECIEVER_EMAIL = "extra_chat_reciever_email";
    public static final String EXTRA_CHAT_RECIEVER_USER_ID = "extra_chat_reciever_userid";
}
