package com.oss.oss.utils;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

/**
 * FragmentManagerUtil - utility class for fragment transaction throughout the application
 *
 * @author Sapun Wiratunga <s.wiratunga@cqumail.com>
 * @version 1.0
 * @since 1.0
 */
public class FragmentManagerUtil {
    public static final String TAG = FragmentManagerUtil.class.getSimpleName();

    /**
     * Constructor
     */
    public FragmentManagerUtil() {
        // TODO Auto-generated constructor stub
    }

    /**
     * Replace fragment
     *
     * @param fm        Fragment manager
     * @param container View container resource id
     * @param target    Target fragment which is going to replace existing fragment
     * @param backstack is to add in backstack
     * @param TAG       Tag for fragment transaction
     */
    public static void replaceFragment(FragmentManager fm, int container, Fragment target, boolean backstack, String TAG) {

        Fragment fragment = target;
        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = fm;
        fragmentManager.executePendingTransactions();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(container, fragment, TAG);
        if (backstack)
            fragmentTransaction.addToBackStack(TAG);
        fragmentTransaction.commit();
    }

    /**
     * Add fragment
     *
     * @param fm        Fragment manager
     * @param container View container resource id
     * @param target    Target fragment which is to be added in container
     * @param backstack is to add in backstack
     * @param TAG       Tag for fragment transaction
     */
    public static void addFragment(FragmentManager fm, int container, Fragment target, boolean backstack, String TAG) {

        Fragment fragment = target;
        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = fm;
        fragmentManager.executePendingTransactions();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(container, fragment, TAG);
        //add to backstack only if backstack flag is true
        if (backstack)
            fragmentTransaction.addToBackStack(TAG);
        fragmentTransaction.commit();
    }
}
