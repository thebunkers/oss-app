package com.oss.oss.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

/**
 * DialogUtils - utility class to show dialog throughout the application
 *
 * @author Sapun Wiratunga <s.wiratunga@cqumail.com>
 * @version 1.0
 * @since 1.0
 */
public class DialogUtils {
    public static final String TAG = DialogUtils.class.getSimpleName();
    public static boolean show = false;

    /**
     * Show dialog.
     *
     * @param ctx      View context
     * @param title    Title of the dialog
     * @param msg      Message body of the dialog
     * @param listener Click listener of Positive button
     */
    public static void showDialog(Context ctx, String title, String msg, String positiveButton, String negativeButton,
                                  DialogInterface.OnClickListener listener) {

        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setMessage(msg).setCancelable(false)
                .setPositiveButton(positiveButton, listener)
                .setNegativeButton(negativeButton, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        builder.setTitle(title);
        AlertDialog alert = builder.create();
        alert.show();
    }


}
