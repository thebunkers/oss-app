package com.oss.oss.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.format.DateUtils;

import com.oss.oss.Rest.model.ModelChats;
import com.oss.oss.home.HomeActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * TagsAndConstants - utility class which contains miscellaneous utils to be used throughout the application
 *
 * @author Sapun Wiratunga <s.wiratunga@cqumail.com>
 * @version 1.0
 * @since 1.0
 */
public class Utils {
    private static ProgressDialog progressDialog;

    /**
     * Show progress dialog to show some remote service call
     *
     * @param context View's context
     */
    public static void showProgressDialog(Context context) {

        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please wait....");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    /**
     * Hide progress dialog which is currently being shown
     */
    public static void hideProgressDialog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing())
                progressDialog.dismiss();
        }
    }

    /**
     * Default shared preference instance
     *
     * @param context View's context
     * @return Default {@link SharedPreferences} instance
     */
    public static SharedPreferences getSharedPreference(Context context) {
        return context.getApplicationContext().getSharedPreferences(TagsAndConstants.PREFERENCE_NAME, Context.MODE_PRIVATE);
    }

    /**
     * Default shared preference editor
     *
     * @param context View's Context
     * @return Default {@link android.content.SharedPreferences.Editor} instance
     */
    public static SharedPreferences.Editor getSharedPreferenceEditor(Context context) {
        return context.getApplicationContext().getSharedPreferences(TagsAndConstants.PREFERENCE_NAME, Context.MODE_PRIVATE).edit();
    }

    /**
     * Calculate time between two time stamps
     *
     * @param targetTimeStamp  Target time stamp
     * @param currentTimeStamp Current time stamp
     * @return Duration between target time stamp and current time stamp
     */
    public static String calculateTimeAgo(long targetTimeStamp, long currentTimeStamp) {
        CharSequence relativeTimeSpanString = DateUtils.getRelativeTimeSpanString(targetTimeStamp, currentTimeStamp, DateUtils.MINUTE_IN_MILLIS);
        return relativeTimeSpanString.toString();
    }

    /**
     * Convert date into milli seconds
     *
     * @param srcDate Date time stamp
     * @return Time stamp in milli seconds
     */
    public static long getDateInMillis(String srcDate) {
        SimpleDateFormat desiredFormat = new SimpleDateFormat(
                "yyyy-MM-dd hh:mm:ss");

        long dateInMillis = 0;
        try {
            Date date = desiredFormat.parse(srcDate);
            dateInMillis = date.getTime();
            return dateInMillis;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return 0;
    }

    public static String getCurrentDate(){
        Date newDate= new Date();
        SimpleDateFormat spf= new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String date = spf.format(newDate);
        return date;
    }

    public static void logoutUser(Context context) {
        Utils.getSharedPreferenceEditor(context).clear().commit();
        ModelChats.setInstance("{}");

    }
}
