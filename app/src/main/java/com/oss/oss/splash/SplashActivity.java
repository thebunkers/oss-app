package com.oss.oss.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.oss.oss.Login.UserActivity;
import com.oss.oss.R;
import com.oss.oss.Rest.DataManager;
import com.oss.oss.Rest.model.ModelAllMsg;
import com.oss.oss.Rest.model.ModelAllPosts;
import com.oss.oss.Rest.model.ModelChats;
import com.oss.oss.Rest.network.RemoteCallback;
import com.oss.oss.Rest.network.RemoteServices;
import com.oss.oss.home.HomeActivity;
import com.oss.oss.utils.TagsAndConstants;
import com.oss.oss.utils.Utils;

import timber.log.Timber;

/**
 * Splash activity- Entry point of the application
 * <p>Downloads posts and messages list from server</p>
 *
 * @author Sapun Wiratunga <s.wiratunga@cqumail.com>
 * @version 1.0
 * @since 1.0
 */
public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        //show app logo and version for at least 1.5 seconds
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                downloadAppData();
//                next();
            }
        }, 1500);
    }

    //Download application data from server
    private void downloadAppData() {
        downloadPostList();
    }

    /**
     * Download list of all posts from server
     *
     * @see RemoteServices#getAllPosts()
     */
    private void downloadPostList() {
        Utils.showProgressDialog(SplashActivity.this);
        DataManager.getInstance().getAllPosts(new RemoteCallback<ModelAllPosts>() {
            @Override
            public void onSuccess(ModelAllPosts response) {
                Timber.i("download posts list: " + new Gson().toJson(response));
                ModelAllPosts.setInstance(response);
                next();
            }

            @Override
            public void onFailed(Throwable throwable) {
                Utils.hideProgressDialog();
                Timber.e(throwable, "download posts list: ");
            }
        });
    }

    /**
     * Navigate to next page after downloading app data
     */
    private void next() {

        //set user's chat singleton if it exist in preference
        if (!TextUtils.isEmpty(Utils.getSharedPreference(SplashActivity.this).getString(TagsAndConstants.TAG_CHATS_JSON, ""))) {//user is not present in preference
            ModelChats.setInstance(Utils.getSharedPreference(SplashActivity.this).getString(TagsAndConstants.TAG_CHATS_JSON, ""));
        }

        Class targetClass;
        if (TextUtils.isEmpty(Utils.getSharedPreference(SplashActivity.this).getString(TagsAndConstants.TAG_USER_JSON, ""))) {//user is not present in preference
            targetClass = UserActivity.class;
        } else {
            targetClass = HomeActivity.class;
        }

        //start target activity
        startActivity(new Intent(SplashActivity.this, targetClass));
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Utils.hideProgressDialog();
    }
}
