package com.oss.oss.admin;

import android.content.Context;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.oss.oss.R;
import com.oss.oss.Rest.DataManager;
import com.oss.oss.Rest.Resources.ResUser;
import com.oss.oss.Rest.model.ModelDeleteUser;
import com.oss.oss.Rest.network.RemoteCallback;
import com.oss.oss.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

/**
 * AdminFragmentAdapter - Adapter class for recyclerview used in {@link AdminFragment}
 *
 * @author Sapun Wiratunga <s.wiratunga@cqumail.com>
 * @version 1.0
 * @since 1.0
 */
public class AdminFragmentAdapter extends RecyclerView.Adapter<AdminFragmentAdapter.ViewHolder> {

    private static SparseBooleanArray checked = new SparseBooleanArray();
    private final List<ResUser> mValues;
    private OnUsersDeletedListner onUserDeletedListener;
    private Context context;

    /**
     * Constructor
     *
     * @param ctx     Context of the parent view
     * @param items   list of model items to be displayed
     * @param listner {@link OnUsersDeletedListner} instance
     */
    public AdminFragmentAdapter(final Context ctx, final List<ResUser> items, OnUsersDeletedListner listner) {
        context = ctx;
        mValues = items;
        onUserDeletedListener = listner;


        AdminFragment.setOnDeleteUserClickListener(new AdminFragment.OnDeleteUserClickListener() {
            @Override
            public void onClickDeleteUser() {


                Toast.makeText(ctx, "delete button clicked triggered in adapter", Toast.LENGTH_SHORT).show();
                List<String> usersToBeDeleted = new ArrayList<String>();
                for (int i = 0; i < mValues.size(); i++) {
                    Timber.d("checked check : total size:" + String.valueOf(checked.size()));
                    Timber.d("checked check : postion " + i + " " + String.valueOf(checked.get(i)));
                    if (checked.get(i)) {
                        usersToBeDeleted.add(String.valueOf(mValues.get(i).getUser_id()));
                    }
                }

                Utils.showProgressDialog(context);
                DataManager.getInstance().deleteUsers(TextUtils.join(",", usersToBeDeleted), new RemoteCallback<ModelDeleteUser>() {
                    @Override
                    public void onSuccess(ModelDeleteUser response) {
                        Utils.hideProgressDialog();
                        Timber.i("Delete users: " + new Gson().toJson(response));
                        Toast.makeText(context, "" + response.getMsg(), Toast.LENGTH_SHORT).show();
                        if (!response.isError()) {
                            if (onUserDeletedListener != null)
                                onUserDeletedListener.onUserDelete();
                        }
                    }

                    @Override
                    public void onFailed(Throwable throwable) {
                        Utils.hideProgressDialog();
                        Timber.e(throwable, "Delete users: ");
                    }
                });
            }
        });
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_admin_frag, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);

        //Bind data with viewholder at current position
        ((ViewHolder) holder).bindData();

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    /**
     * Interface definition for a callback to be invoked when user deleted.
     */
    public interface OnUsersDeletedListner {
        public void onUserDelete();
    }

    /**
     * ViewHolder class for AdminFragmentAdapter row item
     */
    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public ResUser mItem;
        @BindView(R.id.txtItemAdminUser)
        TextView txtItemAdminUser;
        @BindView(R.id.cbItemAdminUser)
        AppCompatCheckBox cbItemAdminUser;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            ButterKnife.bind(this, view);

            cbItemAdminUser.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        checked.put(getAdapterPosition(), true);
                    } else {
                        checked.put(getAdapterPosition(), false);
                    }

                    Timber.d("Checked check: onchackedchange: position " + String.valueOf(getAdapterPosition()) + " " + String.valueOf(checked.get(getAdapterPosition())));
                }
            });
        }

        public void bindData() {
            if (checked.get(getAdapterPosition()))
                cbItemAdminUser.setChecked(true);
            else
                cbItemAdminUser.setChecked(false);

            txtItemAdminUser.setText(mItem.getF_name() + " " + mItem.getL_name());

        }
    }


}
