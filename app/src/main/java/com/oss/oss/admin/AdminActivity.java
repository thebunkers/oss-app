package com.oss.oss.admin;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.oss.oss.R;
import com.oss.oss.utils.FragmentManagerUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Admin activity - Main container for admin related pages and tasks
 *
 * @author Sapun Wiratunga <s.wiratunga@cqumail.com>
 * @version 1.0
 * @since 1.0
 */
public class AdminActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);

        //Butterknif injection for binding views
        ButterKnife.bind(this);

        //setup toolbar
        setSupportActionBar(toolbar);

        //attach admin frgment
        showAdminFragment();

    }

    /**
     * Display admin fragment.
     * <p>attach {@link com.oss.oss.admin.AdminFragment} to {@link com.oss.oss.admin.AdminActivity}</p>
     */
    private void showAdminFragment() {
        FragmentManagerUtil.replaceFragment(getSupportFragmentManager(), R.id.frame_admin, new AdminFragment(), false, AdminFragment.TAG);
    }

}
