package com.oss.oss.admin;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.gson.Gson;
import com.oss.oss.Login.UserActivity;
import com.oss.oss.R;
import com.oss.oss.Rest.DataManager;
import com.oss.oss.Rest.Resources.ResUser;
import com.oss.oss.Rest.model.ModelAllUsers;
import com.oss.oss.Rest.model.ModelUser;
import com.oss.oss.Rest.network.RemoteCallback;
import com.oss.oss.Rest.network.RemoteServices;
import com.oss.oss.home.HomeActivity;
import com.oss.oss.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import timber.log.Timber;

/**
 * Admin activity - Main container for admin related pages and tasks
 * @author Sapun Wiratunga <s.wiratunga@cqumail.com>
 * @version 1.0
 * @since 1.0
 */
public class AdminFragment extends Fragment {

    /**
     * Admin fragment's tag
     */
    public static final String TAG = AdminFragment.class.getSimpleName();
    private static OnDeleteUserClickListener onDeleteUserClickListenerListener;
    @BindView(R.id.list)
    RecyclerView list;
    Unbinder unbinder;
    @BindView(R.id.btnAdminFragLogout)
    Button btnAdminFragLogout;
    @BindView(R.id.btnAdminFragDeleteUsers)
    Button btnAdminFragDeleteUsers;
    @BindView(R.id.btnAdminFragHome)
    ImageButton btnAdminFragHome;

    List<ResUser> userList=new ArrayList<>();
    private AdminFragmentAdapter adapter;

    public AdminFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_admin, container, false);
        //butterknif injection for binding views
        unbinder = ButterKnife.bind(this, view);

        //setup users list
        setupList();

        return view;
    }

    /**
     * Setup users list
     */
    private void setupList() {
        list.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new AdminFragmentAdapter(getActivity(), userList, new AdminFragmentAdapter.OnUsersDeletedListner() {
            @Override
            public void onUserDelete() {
                downloadUsers();
            }
        });
        list.setAdapter(adapter);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //Download user's list from server
        downloadUsers();
    }

    /**
     * Doanlod list of users
     * @see RemoteServices#getAllUsers()
     */
    private void downloadUsers() {
        Utils.showProgressDialog(getActivity());
        DataManager.getInstance().getAllUsers(ModelUser.getInstance().getUser().getUser_id(), new RemoteCallback<ModelAllUsers>() {
            @Override
            public void onSuccess(ModelAllUsers response) {
                Utils.hideProgressDialog();
                Timber.i("downloadUsersList: "+new Gson().toJson(response));

                if(!response.isError()){
                    userList.clear();
                    userList.addAll(response.getUsers());
                    adapter.notifyDataSetChanged();
                }

            }

            @Override
            public void onFailed(Throwable throwable) {
                Utils.hideProgressDialog();
                Timber.e(throwable,"downloadUsersList: ");
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //butterknif unbind views
        unbinder.unbind();
    }

    @OnClick({R.id.btnAdminFragLogout, R.id.btnAdminFragDeleteUsers, R.id.btnAdminFragHome})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnAdminFragLogout:
                //clear all preferences
                Utils.logoutUser(getActivity());
                startActivity(new Intent(getActivity(),UserActivity.class));
                getActivity().finish();
                break;
            case R.id.btnAdminFragDeleteUsers:
                    if(onDeleteUserClickListenerListener !=null)
                        onDeleteUserClickListenerListener.onClickDeleteUser();
                Toast.makeText(getActivity(), "delete button clicked triggered in fragment", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btnAdminFragHome:
                startActivity(new Intent(getActivity(),HomeActivity.class));
                getActivity().finish();
                break;
        }
    }

    /**
     * Register a callback to be invoked when delete user button is clicked.
     * @param listener
     * OnDeleteUserClickListener instance
     * @see AdminFragment.OnDeleteUserClickListener
     */
    public static void setOnDeleteUserClickListener(OnDeleteUserClickListener listener){
        onDeleteUserClickListenerListener = listener;
    }

    /**
     * Interface definition for a callback to be invoked when a Delete button is clicked.
     */
    public interface OnDeleteUserClickListener {
        public void onClickDeleteUser();
    }
}
