package com.oss.oss.Rest.model;

import com.oss.oss.Rest.Resources.ResMsg;

import java.util.List;

public class ModelAllMsg {


    /**
     * error : false
     * msg : Your msg sent successfully
     * message : [{"msg_id":"31","msg_text":"this is secound msz","sent_time":"2017-05-18 16:48:42","sent_by":"1","send_to":"2"},{"msg_id":"30","msg_text":"this is first msz","sent_time":"2017-05-18 16:48:37","sent_by":"1","send_to":"2"}]
     */

    private boolean error;
    private String msg;
    private List<ResMsg> message;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<ResMsg> getMessage() {
        return message;
    }

    public void setMessage(List<ResMsg> message) {
        this.message = message;
    }


}
