package com.oss.oss.Rest.Resources;

public class ResUser {

    private String user_id;
    private String user_type;
    private String f_name;
    private String l_name;
    private String gender;
    private String dob;
    private String email;
    private String address;
    private String university;
    private String visa_grant_date;
    private String visa_expiry_date;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public String getF_name() {
        return f_name;
    }

    public void setF_name(String f_name) {
        this.f_name = f_name;
    }

    public String getL_name() {
        return l_name;
    }

    public void setL_name(String l_name) {
        this.l_name = l_name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public String getVisa_grant_date() {
        return visa_grant_date;
    }

    public void setVisa_grant_date(String visa_grant_date) {
        this.visa_grant_date = visa_grant_date;
    }

    public String getVisa_expiry_date() {
        return visa_expiry_date;
    }

    public void setVisa_expiry_date(String visa_expiry_date) {
        this.visa_expiry_date = visa_expiry_date;
    }
}
