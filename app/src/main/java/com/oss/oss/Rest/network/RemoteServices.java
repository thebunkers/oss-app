/*
 * Copyright (c) Joaquim Ley 2016. All Rights Reserved.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.oss.oss.Rest.network;

import com.oss.oss.Rest.model.ModelAllMsg;
import com.oss.oss.Rest.model.ModelAllPosts;
import com.oss.oss.Rest.model.ModelAllUsers;
import com.oss.oss.Rest.model.ModelChats;
import com.oss.oss.Rest.model.ModelDeletePost;
import com.oss.oss.Rest.model.ModelDeleteUser;
import com.oss.oss.Rest.model.ModelNewMsg;
import com.oss.oss.Rest.model.ModelNewPost;
import com.oss.oss.Rest.model.ModelUser;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * RemoteServices- Service interface which holds all service endpoints
 *
 * @author Sapun Wiratunga <s.wiratunga@cqumail.com>
 * @version 1.0
 * @since 1.0
 */
public interface RemoteServices {


    @FormUrlEncoded
    @POST("register.php")
    Call<ModelUser> register(
            @Field("f_name") String f_name,
            @Field("l_name") String l_name,
            @Field("gender") String gender,
            @Field("dob") String dob,
            @Field("email") String email,
            @Field("pass") String pass,
            @Field("address") String address,
            @Field("university") String university,
            @Field("visa_grant_date") String visa_grant_date,
            @Field("visa_expiry_date") String visa_expiry_date
    );

    @FormUrlEncoded
    @POST("login.php")
    Call<ModelUser> login(
            @Field("email") String email,
            @Field("password") String password
    );

    @GET("msgs.php")
    Call<ModelAllMsg> getAllMsgs();

    @FormUrlEncoded
    @POST("sendmsg.php")
    Call<ModelNewMsg> sendMsg(
            @Field("msg_text") String msg_text,
            @Field("user_id") String user_id,
            @Field("sent_time") String sent_time,
            @Field("send_to") String send_to
    );
    @Multipart
    @POST("newpost.php")
    Call<ModelNewPost> sendPost(@Part("type") RequestBody type,
                                @Part("created_by") RequestBody created_by,
                                @Part("title") RequestBody title,
                                @Part("post_text") RequestBody post_text,
                                @Part MultipartBody.Part file);

    @GET("posts.php")
    Call<ModelAllPosts> getAllPosts();

    @FormUrlEncoded
    @POST("users.php")
    Call<ModelAllUsers> getAllUsers(@Field("user_id") String userID);

    @FormUrlEncoded
    @POST("deletepost.php")
    Call<ModelDeletePost> deletePost(
            @Field("post_id") String post_id
    );

    @FormUrlEncoded
    @POST("deleteuser.php")
    Call<ModelDeleteUser> deleteUsers(
            @Field("user_id") String user_id
    );

    @FormUrlEncoded
    @POST("getchats.php")
    Call<ModelChats> getChats(
            @Field("user_id") String user_id
    );

    @FormUrlEncoded
    @POST("getmessages.php")
    Call<ModelAllMsg> getMessagesWithUser(
            @Field("current_user") String current_user_id,
            @Field("target_user") String recipient_user_id
    );


}


