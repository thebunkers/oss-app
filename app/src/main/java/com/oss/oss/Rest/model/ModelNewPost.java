package com.oss.oss.Rest.model;

import com.oss.oss.Rest.Resources.ResPost;

public class ModelNewPost {

    private boolean error;
    private String message;
    private ResPost post;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResPost getPost() {
        return post;
    }

    public void setPost(ResPost post) {
        this.post = post;
    }

}
