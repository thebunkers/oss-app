package com.oss.oss.Rest.model;

import com.oss.oss.Rest.Resources.ResUser;

import java.util.List;

public class ModelAllUsers {

    private boolean error;
    private String msg;
    private List<ResUser> users;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<ResUser> getUsers() {
        return users;
    }

    public void setUsers(List<ResUser> users) {
        this.users = users;
    }

}
