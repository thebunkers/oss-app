package com.oss.oss.Rest.Resources;

public class ResChat {


    /**
     * sent_by : 3
     * user : {"user_id":"3","user_type":"user","f_name":"Supun","l_name":"W","gender":"female","dob":"2010-02-01","email":"slakshan92@gmail.com","pass":"1234","address":"Bjdhd","university":"cqu","visa_grant_date":"2010-02-01","visa_expiry_date":"2013-02-01"}
     * last_msz : {"msg_text":"this is third msz","sent_time":"2017-05-18 16:56:25"}
     */

    private String sent_by;
    private ResUser user;
    private LastMszBean last_msz;

    public String getSent_by() {
        return sent_by;
    }

    public void setSent_by(String sent_by) {
        this.sent_by = sent_by;
    }

    public ResUser getUser() {
        return user;
    }

    public void setUser(ResUser user) {
        this.user = user;
    }

    public LastMszBean getLast_msz() {
        return last_msz;
    }

    public void setLast_msz(LastMszBean last_msz) {
        this.last_msz = last_msz;
    }


    public static class LastMszBean {
        /**
         * msg_text : this is third msz
         * sent_time : 2017-05-18 16:56:25
         */

        private String msg_text;
        private String sent_time;

        public String getMsg_text() {
            return msg_text;
        }

        public void setMsg_text(String msg_text) {
            this.msg_text = msg_text;
        }

        public String getSent_time() {
            return sent_time;
        }

        public void setSent_time(String sent_time) {
            this.sent_time = sent_time;
        }
    }
}
