/*
 * Copyright (c) Joaquim Ley 2016. All Rights Reserved.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.oss.oss.Rest.network;

import com.oss.oss.utils.TagsAndConstants;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * RemoteServicesFactory- Service Factory class which will provide service to rest of the application
 *
 * @author Sapun Wiratunga <s.wiratunga@cqumail.com>
 * @version 1.0
 * @since 1.0
 */
public class RemoteServicesFactory {



    private static final String BASE_URL = ""+ TagsAndConstants.DOMAIN_NAME+"/api/";
    private static final int HTTP_READ_TIMEOUT = 10000;
    private static final int HTTP_CONNECT_TIMEOUT = 6000;

    /**
     * Get instance of RemoteService
     * @return
     * {@link #getServices()}
     */
    public static RemoteServices getServices() {
        return getServices(makeOkHttpClient());
    }

    /**
     * Get instance of RemoteService
     * @return
     * new {@link RemoteServices} instance
     */
    private static RemoteServices getServices(OkHttpClient okHttpClient) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();

        return retrofit.create(RemoteServices.class);
    }

    /**
     * OkHttpClient
     * @return
     * new {@link OkHttpClient} instance
     */
    private static OkHttpClient makeOkHttpClient() {

        OkHttpClient.Builder httpClientBuilder = new OkHttpClient().newBuilder();
        httpClientBuilder.connectTimeout(HTTP_CONNECT_TIMEOUT, TimeUnit.SECONDS);
        httpClientBuilder.readTimeout(HTTP_READ_TIMEOUT, TimeUnit.SECONDS);
        httpClientBuilder.addInterceptor(makeLoggingInterceptor());

        return httpClientBuilder.build();
    }

    /**
     * Custom HTTP Logger to log Request body
     * @return
     * new {@link HttpLoggingInterceptor} instance
     */
    private static HttpLoggingInterceptor makeLoggingInterceptor() {


        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        return logging;
    }


}