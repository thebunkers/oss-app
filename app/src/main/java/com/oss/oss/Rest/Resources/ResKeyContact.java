package com.oss.oss.Rest.Resources;

import java.util.List;

/**
 * Created by ReconAppMagic on 16/05/17.
 */

public class ResKeyContact {


    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * name : Australian consumer law
         * when_to_use : Understand your legal rights in to shopping or purchasing services.
         * contact : consumerlaw.gov.au
         */

        private String name;
        private String when_to_use;
        private String contact;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getWhen_to_use() {
            return when_to_use;
        }

        public void setWhen_to_use(String when_to_use) {
            this.when_to_use = when_to_use;
        }

        public String getContact() {
            return contact;
        }

        public void setContact(String contact) {
            this.contact = contact;
        }
    }
}
