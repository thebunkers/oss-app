package com.oss.oss.Rest.model;

import com.google.gson.Gson;
import com.oss.oss.Rest.Resources.ResChat;

import java.util.List;

public class ModelChats {

    private static ModelChats mInstance;

    public static ModelChats getInstance(){
        return mInstance;
    }

    public static void setInstance( String chatsJson) {
        mInstance=new Gson().fromJson(chatsJson,ModelChats.class);
    }

    public static void setInstance(ModelChats instance) {

        mInstance = instance;
    }


    /**
     * error : false
     * msg : Your msg sent successfully
     * chats : [{"sent_by":"3","user":{"user_id":"3","user_type":"user","f_name":"Supun","l_name":"W","gender":"female","dob":"2010-02-01","email":"slakshan92@gmail.com","pass":"1234","address":"Bjdhd","university":"cqu","visa_grant_date":"2010-02-01","visa_expiry_date":"2013-02-01"},"last_msz":{"msg_text":"this is third msz","sent_time":"2017-05-18 16:56:25"}},{"sent_by":"2","user":{"user_id":"2","user_type":"user","f_name":"Sss","l_name":"sss","gender":"female","dob":"2010-02-19","email":"androidsuppa@gmail.com","pass":"1111","address":"Bdjs","university":"cdvs","visa_grant_date":"2010-02-11","visa_expiry_date":"2010-02-27"},"last_msz":{"msg_text":"this is secound msz","sent_time":"2017-05-18 16:48:42"}}]
     */

    private boolean error;
    private String msg;
    private List<ResChat> chats;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<ResChat> getChats() {
        return chats;
    }

    public void setChats(List<ResChat> chats) {
        this.chats = chats;
    }


}
