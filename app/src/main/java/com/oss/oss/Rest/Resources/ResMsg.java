package com.oss.oss.Rest.Resources;

public class ResMsg {


    /**
     * msg_id : 31
     * msg_text : this is secound msz
     * sent_time : 2017-05-18 16:48:42
     * sent_by : 1
     * send_to : 2
     */

    private String msg_id;
    private String msg_text;
    private String sent_time;
    private String sent_by;
    private String send_to;

    public String getMsg_id() {
        return msg_id;
    }

    public void setMsg_id(String msg_id) {
        this.msg_id = msg_id;
    }

    public String getMsg_text() {
        return msg_text;
    }

    public void setMsg_text(String msg_text) {
        this.msg_text = msg_text;
    }

    public String getSent_time() {
        return sent_time;
    }

    public void setSent_time(String sent_time) {
        this.sent_time = sent_time;
    }

    public String getSent_by() {
        return sent_by;
    }

    public void setSent_by(String sent_by) {
        this.sent_by = sent_by;
    }

    public String getSend_to() {
        return send_to;
    }

    public void setSend_to(String send_to) {
        this.send_to = send_to;
    }
}
