/*
 * Copyright (c) Joaquim Ley 2016. All Rights Reserved.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.oss.oss.Rest;

import com.oss.oss.Rest.model.ModelAllMsg;
import com.oss.oss.Rest.model.ModelAllPosts;
import com.oss.oss.Rest.model.ModelAllUsers;
import com.oss.oss.Rest.model.ModelChats;
import com.oss.oss.Rest.model.ModelDeletePost;
import com.oss.oss.Rest.model.ModelDeleteUser;
import com.oss.oss.Rest.model.ModelNewMsg;
import com.oss.oss.Rest.model.ModelNewPost;
import com.oss.oss.Rest.model.ModelUser;
import com.oss.oss.Rest.network.RemoteServices;
import com.oss.oss.Rest.network.RemoteServicesFactory;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Callback;

/**
 * DataManager- Data layer implementation
 *
 * @author Sapun Wiratunga <s.wiratunga@cqumail.com>
 * @version 1.0
 * @since 1.0
 */
public class DataManager {

    private static DataManager sInstance;

    private final RemoteServices service;

    /**
     * Constructor
     */
    private DataManager() {
        service = RemoteServicesFactory.getServices();
    }

    /**
     * Instance getter
     *
     * @return new instance of {@link DataManager}
     */
    public static DataManager getInstance() {
        if (sInstance == null) {
            sInstance = new DataManager();
        }
        return sInstance;
    }

    /**
     * Register user
     *
     * @param fname       first name of user
     * @param lname       Last name of user
     * @param gender      Gender of user
     * @param dob         Date of Birth of user
     * @param email       Email of user
     * @param pass        Password
     * @param address     Address of user
     * @param university  University of user
     * @param visa_grant  Visa grant date
     * @param visa_expiry Visa expiry date
     * @param listener    Data callback
     */
    public void register(String fname, String lname, String gender, String dob, String email, String pass, String address, String university, String visa_grant, String visa_expiry, Callback<ModelUser> listener) {
        service.register(fname, lname, gender, dob, email, pass, address, university, visa_grant, visa_expiry)
                .enqueue(listener);
    }

    /**
     * Login
     *
     * @param email    User name
     * @param pass     Password
     * @param listener Data callback
     */
    public void login(String email, String pass, Callback<ModelUser> listener) {
        service.login(email, pass)
                .enqueue(listener);
    }

    /**
     * Send new message to server
     * @param msgText  Message body
     * @param userId   User's id who is sending this message
     * @param sent_time
     * @param send_to
     * @param listener Data callback
     */
    public void sendMsg(String msgText, String userId, String sent_time, String send_to, Callback<ModelNewMsg> listener) {
        service.sendMsg(msgText, String.valueOf(userId),sent_time,send_to)
                .enqueue(listener);
    }

    /**
     * Send new post
     *
     * @param type       Post type
     * @param created_by Current user's id
     * @param title      Title of the post
     * @param post_text  Body of the post
     * @param image      Post image
     * @param listener   Data listener
     */
    public void sendPost(RequestBody type, RequestBody created_by, RequestBody title, RequestBody post_text, MultipartBody.Part image, Callback<ModelNewPost> listener) {
        service.sendPost(type, created_by, title, post_text, image)
                .enqueue(listener);
    }

    /**
     * Get list of all posts from server
     *
     * @param listener Data callback
     */
    public void getAllPosts(Callback<ModelAllPosts> listener) {
        service.getAllPosts()
                .enqueue(listener);
    }

    /**
     * get list of all users
     *
     * @param userId
     * current user's id
     * @param listener Data callback
     */
    public void getAllUsers(String userId, Callback<ModelAllUsers> listener) {
        service.getAllUsers(userId)
                .enqueue(listener);
    }

    /**
     * Delete post
     *
     * @param post_id  Post's id which is to be deleted
     * @param listener Data callback
     */
    public void deletePost(String post_id, Callback<ModelDeletePost> listener) {
        service.deletePost(String.valueOf(post_id))
                .enqueue(listener);
    }

    /**
     * Delete user
     *
     * @param user_id  Current user's id
     * @param listener Data callback
     */
    public void deleteUsers(String user_id, Callback<ModelDeleteUser> listener) {
        service.deleteUsers(user_id)
                .enqueue(listener);
    }

    /**
     * get all Chats of current logged in user
     * @param userid
     * current user's userid
     * @param listener
     * Data callback
     */
    public void getChats(String userid, Callback<ModelChats> listener){
        service.getChats(userid)
                .enqueue(listener);
    }


    /**
     * Get all messages
     * @param current_userId
     * current user's userId
     * @param recipient_userId
     * user id of that user with which current user is chating
     * @param listener Data callback
     */
    public void getMessagesWithUser(String current_userId, String recipient_userId, Callback<ModelAllMsg> listener) {
        service.getMessagesWithUser(current_userId,recipient_userId)
                .enqueue(listener);
    }

}