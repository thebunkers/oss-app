package com.oss.oss.Rest.model;

import com.google.gson.Gson;
import com.oss.oss.Rest.Resources.ResPost;

import java.util.List;

public class ModelAllPosts {

    private static ModelAllPosts mInstance;

    public static ModelAllPosts getInstance(){
        return mInstance;
    }

    public static void setInstance(String postsJson) {
        mInstance=new Gson().fromJson(postsJson,ModelAllPosts.class);
    }

    public static void setInstance(ModelAllPosts instance) {

        mInstance = instance;
    }

    private boolean error;
    private String msg;
    private List<ResPost> posts;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<ResPost> getPosts() {
        return posts;
    }

    public void setPosts(List<ResPost> posts) {
        this.posts = posts;
    }

}
