package com.oss.oss.Rest.model;

public class ModelDeletePost {


    /**
     * error : false
     * msg : Post deleted successfully
     */

    private boolean error;
    private String msg;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
