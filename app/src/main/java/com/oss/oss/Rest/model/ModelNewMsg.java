package com.oss.oss.Rest.model;

import com.oss.oss.Rest.Resources.ResMsg;
import com.oss.oss.Rest.Resources.ResUser;

public class ModelNewMsg {


    /**
     * error : false
     * msg : Your msg sent successfully
     * message : {"msg_id":"66","msg_text":"FINAL API TESTING","sent_time":"2017-05-20 02:00:34","sent_by":"2","send_to":"2"}
     * user : {"user_id":"2","user_type":"user","f_name":"Sss","l_name":"sss","gender":"female","dob":"2010-02-19","email":"androidsuppa@gmail.com","pass":"1111","address":"Bdjs","university":"cdvs","visa_grant_date":"2010-02-11","visa_expiry_date":"2010-02-27"}
     */

    private boolean error;
    private String msg;
    private ResMsg message;
    private ResUser user;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResMsg getMessage() {
        return message;
    }

    public void setMessage(ResMsg message) {
        this.message = message;
    }

    public ResUser getUser() {
        return user;
    }

    public void setUser(ResUser user) {
        this.user = user;
    }


}
