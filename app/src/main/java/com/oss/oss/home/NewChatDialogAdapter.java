package com.oss.oss.home;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oss.oss.R;
import com.oss.oss.Rest.Resources.ResUser;
import com.oss.oss.chat.ChatActivity;
import com.oss.oss.utils.TagsAndConstants;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * NewChatDialogAdapter - adapter for new chat dialog user's list
 *
 * @author Sapun Wiratunga <s.wiratunga@cqumail.com>
 * @version 1.0
 * @since 1.0
 */
public class NewChatDialogAdapter extends RecyclerView.Adapter<NewChatDialogAdapter.ViewHolder> {


    private final List<ResUser> mValues;
    private FragmentActivity context;

    /**
     * Constructor
     *
     * @param ctx   View's context
     * @param items
     */
    public NewChatDialogAdapter(FragmentActivity ctx, List<ResUser> items) {
        context = ctx;
        mValues = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_new_chat_dialog, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        ((ViewHolder) holder).bindDate();

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    /**
     * View holder for chat list item
     */
    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public ResUser mItem;
        @BindView(R.id.txtItemNewChatName)
        TextView txtItemNewChatName;
        @BindView(R.id.txtItemNewChatEmail)
        TextView txtItemNewChatEmail;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            ButterKnife.bind(this, view);

            mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent toChatActivity= new Intent(context,ChatActivity.class);
                    toChatActivity.putExtra(TagsAndConstants.EXTRA_CHAT_RECIEVER_NAME,mItem.getF_name()+ " "+ mItem.getL_name());
                    toChatActivity.putExtra(TagsAndConstants.EXTRA_CHAT_RECIEVER_EMAIL,mItem.getEmail());
                    toChatActivity.putExtra(TagsAndConstants.EXTRA_CHAT_RECIEVER_USER_ID,mItem.getUser_id());
                    context.startActivity(toChatActivity);
                }
            });
        }

        public void bindDate() {
            txtItemNewChatName.setText(mItem.getF_name() + " " + mItem.getL_name());
            txtItemNewChatEmail.setText(mItem.getEmail());
        }
    }
}
