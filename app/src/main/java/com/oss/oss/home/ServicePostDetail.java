package com.oss.oss.home;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.oss.oss.R;
import com.oss.oss.Rest.Resources.ResPost;
import com.oss.oss.utils.TagsAndConstants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * ServicePostDetail - Post's detail fragment which shows detail about a particular service
 *
 * @author Sapun Wiratunga <s.wiratunga@cqumail.com>
 * @version 1.0
 * @since 1.0
 */
public class ServicePostDetail extends Fragment {


    public static final String TAG = ServicePostDetail.class.getSimpleName();
    private static ResPost post;
    @BindView(R.id.txtServiceDetailType)
    TextView txtServiceDetailType;
    @BindView(R.id.txtServiceDetailPostTitle)
    TextView txtServiceDetailPostTitle;
    @BindView(R.id.imgServiceDetailPostImage)
    ImageView imgServiceDetailPostImage;
    @BindView(R.id.txtServiceDetailPostDescription)
    TextView txtServiceDetailPostDescription;
    Unbinder unbinder;

    /**
     * Constructor
     */
    public ServicePostDetail() {
        // Required empty public constructor
    }


    /**
     * Parameterized constructor
     *
     * @param mItem Post model item
     * @return new instance of this fragment
     */
    public static ServicePostDetail getInstance(ResPost mItem) {
        post = mItem;
        return new ServicePostDetail();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_service_post_detail, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //show post's details
        showPostDetails();
    }

    /**
     * Shows post post details
     */
    private void showPostDetails() {
        txtServiceDetailType.setText(post.getType());
        txtServiceDetailPostTitle.setText(post.getTitle());
        txtServiceDetailPostDescription.setText(post.getPost_text());

        if (post.getPost_image() != null) {
            Glide.with(getActivity())
                    .load(TagsAndConstants.DOMAIN_NAME + post.getPost_image())
                    .into(imgServiceDetailPostImage);
        } else {
            int imgRrc = 0;
            switch (post.getType()) {
                case TagsAndConstants.SERVICE_TYPE_ACCOMODATION:
                    imgRrc = R.drawable.img_accomodation;
                    break;
                case TagsAndConstants.SERVICE_TYPE_CAREER_SUPPORT:
                    imgRrc = R.drawable.img_career;
                    break;
                case TagsAndConstants.SERVICE_TYPE_CONSULTANCY:
                    imgRrc = R.drawable.img_consultancy;
                    break;
            }
            imgServiceDetailPostImage.setImageResource(imgRrc);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
