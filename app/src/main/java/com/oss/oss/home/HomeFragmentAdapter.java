package com.oss.oss.home;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.oss.oss.R;
import com.oss.oss.Rest.DataManager;
import com.oss.oss.Rest.Resources.ResPost;
import com.oss.oss.Rest.model.ModelDeletePost;
import com.oss.oss.Rest.model.ModelUser;
import com.oss.oss.Rest.network.RemoteCallback;
import com.oss.oss.Services.ServiceActivity;
import com.oss.oss.utils.DialogUtils;
import com.oss.oss.utils.TagsAndConstants;
import com.oss.oss.utils.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

/**
 * HomeFragmentAdapter - Main view of home activity extends {@link android.support.v7.widget.RecyclerView.Adapter}
 *
 * @author Sapun Wiratunga <s.wiratunga@cqumail.com>
 * @version 1.0
 * @since 1.0
 */
public class HomeFragmentAdapter extends RecyclerView.Adapter<HomeFragmentAdapter.ViewHolder> {

    private final List<ResPost> mValues;
    private Context context;

    /**
     * Constructor
     *
     * @param ctx   Context from where it gets invoked
     * @param items list of Posts
     */
    public HomeFragmentAdapter(Context ctx, List<ResPost> items) {
        context = ctx;
        mValues = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_home_frag, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        //bind post item with viewholder
        ((ViewHolder) holder).bindData();

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    /**
     * View holder for post items extends {@link ViewHolder}
     */
    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public ResPost mItem;
        @BindView(R.id.txtItemHomePost)
        TextView txtItemHomePost;
        @BindView(R.id.txtItemHomeTime)
        TextView txtItemHomeTime;

        public ViewHolder(View view) {
            super(view);
            mView = view;

            //Butterknif injection
            ButterKnife.bind(this, view);

            mView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (ModelUser.getInstance().getUser().getUser_type().equalsIgnoreCase(TagsAndConstants.USER_TYPE_ADMIN)) {


                        DialogUtils.showDialog(context, "Delete post", "Are you sure you want to delete this post", "Delete post", "Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Utils.showProgressDialog(context);
                                DataManager.getInstance().deletePost(mItem.getPost_id(), new RemoteCallback<ModelDeletePost>() {
                                    @Override
                                    public void onSuccess(ModelDeletePost response) {
                                        Utils.hideProgressDialog();
                                        Timber.i("DeletePost: " + new Gson().toJson(response));

                                        if (!response.isError()) {//post deleted successfully
                                            mValues.remove(getAdapterPosition());
                                            HomeFragmentAdapter.this.notifyItemRemoved(getAdapterPosition());
                                        }
                                    }

                                    @Override
                                    public void onFailed(Throwable throwable) {
                                        Utils.hideProgressDialog();
                                        Timber.e(throwable, "DeletePost: ");
                                    }
                                });
                            }
                        });
                    }
                    return true;
                }
            });

            mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ServiceActivity.startPostDetailWith(context,mItem);
                }
            });
        }

        public void bindData() {

            txtItemHomePost.setText(String.format("%s posted on \"%s\"", mItem.getUser().getF_name(), mItem.getType()));
            txtItemHomeTime.setText(Utils.calculateTimeAgo(Utils.getDateInMillis(mItem.getCreated_on()), System.currentTimeMillis()));
        }
    }
}
