package com.oss.oss.home;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oss.oss.R;
import com.oss.oss.Rest.DataManager;
import com.oss.oss.Rest.Resources.ResChat;
import com.oss.oss.Rest.Resources.ResMsg;
import com.oss.oss.Rest.model.ModelChats;
import com.oss.oss.Rest.model.ModelUser;
import com.oss.oss.Rest.network.RemoteCallback;
import com.oss.oss.Rest.network.RemoteServices;
import com.oss.oss.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import timber.log.Timber;

/**
 * ChatThreadsFragment - Chat fragment which holds all chat threads
 *
 * @author Sapun Wiratunga <s.wiratunga@cqumail.com>
 * @version 1.0
 * @since 1.0
 */
public class ChatThreadsFragment extends Fragment {

    @BindView(R.id.list)
    RecyclerView list;
    Unbinder unbinder;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.txtDataStatus)
    TextView txtDataStatus;
    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefresh;
    List<ResChat> msgsList = new ArrayList<>();
    private ChatThreadsFragmentAdapter adapter;

    /**
     * Default constructor
     */
    public ChatThreadsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chats, container, false);
        unbinder = ButterKnife.bind(this, view);

        //show previous messages
        showMessages();

        return view;
    }

    /**
     * Show previous messaes form preferences if exist
     */
    private void showMessages() {
        if (ModelChats.getInstance() != null) {//message list already downloaded at Splash
            if (ModelChats.getInstance().getChats() != null) {
                list.setVisibility(View.VISIBLE);
                txtDataStatus.setVisibility(View.GONE);
                msgsList.addAll(ModelChats.getInstance().getChats());
            }
        }
        list.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new ChatThreadsFragmentAdapter(getActivity(), msgsList);
        list.setAdapter(adapter);

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                downloadChats();
            }
        });
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    /**
     * Download messages form server
     *
     * @see RemoteServices#getAllMsgs()
     */
    private void downloadChats() {
        Utils.showProgressDialog(getActivity());
        DataManager.getInstance().getChats(ModelUser.getInstance().getUser().getUser_id(), new RemoteCallback<ModelChats>() {
            @Override
            public void onSuccess(ModelChats response) {
                swipeRefresh.setRefreshing(false);
                Utils.hideProgressDialog();
                Timber.i("Download chat" + new Gson().toJson(response));
                if (response.getChats() != null) {

                    list.setVisibility(View.VISIBLE);
                    txtDataStatus.setVisibility(View.GONE);

                    msgsList.clear();
                    msgsList.addAll(response.getChats());
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailed(Throwable throwable) {
                swipeRefresh.setRefreshing(false);
                Utils.hideProgressDialog();
                Timber.e(throwable, "Download chat");
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.fab)
    public void onViewClicked() {
        NewChatDialog newChatDialog = NewChatDialog.getInstance();
        newChatDialog.show(getActivity().getSupportFragmentManager(), NewChatDialog.TAG);
    }

    /**
     * Add newly added message to current list
     *
     * @param message Newly added message retruned from server
     */
    private void addNewChatToList(ResMsg message) {
//        ResChat chat = new ResChat();
//        chat.setMsg_id(message.getMsg_id());
//        chat.setMsg_text(message.getMsg_text());
//        chat.setSent_time(message.getSent_time());
//        chat.setSent_by(message.getSent_by());
//        chat.setUser(new ResUser());
//        msgsList.add(chat);
//        adapter.notifyItemInserted(msgsList.size() - 1);
//
//        list.setVisibility(View.VISIBLE);
//        txtDataStatus.setVisibility(View.GONE);
    }


}
