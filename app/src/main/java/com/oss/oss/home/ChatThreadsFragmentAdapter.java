package com.oss.oss.home;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oss.oss.R;
import com.oss.oss.Rest.Resources.ResChat;
import com.oss.oss.chat.ChatActivity;
import com.oss.oss.utils.TagsAndConstants;
import com.oss.oss.utils.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * ChatThreadsFragmentAdapter - adapter for chats list
 *
 * @author Sapun Wiratunga <s.wiratunga@cqumail.com>
 * @version 1.0
 * @since 1.0
 */
public class ChatThreadsFragmentAdapter extends RecyclerView.Adapter<ChatThreadsFragmentAdapter.ViewHolder> {


    private final List<ResChat> mValues;
    private FragmentActivity context;

    /**
     * Constructor
     *
     * @param ctx   View's context
     * @param items
     */
    public ChatThreadsFragmentAdapter(FragmentActivity ctx, List<ResChat> items) {
        context = ctx;
        mValues = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_chat_frag, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        ((ViewHolder) holder).bindDate();

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    /**
     * View holder for chat list item
     */
    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public ResChat mItem;
        @BindView(R.id.txtItemChatUserName)
        TextView txtItemChatUserName;
        @BindView(R.id.txtItemChatMsg)
        TextView txtItemChatMsg;
        @BindView(R.id.txtItemChatDate)
        TextView txtItemChatDate;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            ButterKnife.bind(this, view);

            mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent toChatActivity= new Intent(context,ChatActivity.class);
                    toChatActivity.putExtra(TagsAndConstants.EXTRA_CHAT_RECIEVER_NAME,mItem.getUser().getF_name()+ " "+ mItem.getUser().getL_name());
                    toChatActivity.putExtra(TagsAndConstants.EXTRA_CHAT_RECIEVER_EMAIL,mItem.getUser().getEmail());
                    toChatActivity.putExtra(TagsAndConstants.EXTRA_CHAT_RECIEVER_USER_ID,mItem.getUser().getUser_id());
                    context.startActivity(toChatActivity);
                }
            });
        }


        public void bindDate() {

            txtItemChatMsg.setText(mItem.getLast_msz().getMsg_text());
            String relativeTime = Utils.calculateTimeAgo(Utils.getDateInMillis(mItem.getLast_msz().getSent_time()), System.currentTimeMillis());
            txtItemChatDate.setText(String.format("%s", relativeTime));
            txtItemChatUserName.setText(mItem.getUser().getF_name()+" "+mItem.getUser().getL_name());
        }
    }
}
