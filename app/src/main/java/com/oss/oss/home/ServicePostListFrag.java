package com.oss.oss.home;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oss.oss.R;
import com.oss.oss.Rest.DataManager;
import com.oss.oss.Rest.Resources.ResPost;
import com.oss.oss.Rest.model.ModelAllPosts;
import com.oss.oss.Rest.network.RemoteCallback;
import com.oss.oss.Rest.network.RemoteServices;
import com.oss.oss.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import timber.log.Timber;

/**
 * ServicePostListFrag - list fragment which show selected service type
 *
 * @author Sapun Wiratunga <s.wiratunga@cqumail.com>
 * @version 1.0
 * @since 1.0
 */
public class ServicePostListFrag extends Fragment {

    public static final String TAG = ServicePostListFrag.class.getSimpleName();
    private static String serviceType;
    @BindView(R.id.list)
    RecyclerView list;
    Unbinder unbinder;
    List<ResPost> postList = new ArrayList<>();
    @BindView(R.id.txtDataStatus)
    TextView txtDataStatus;
    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.txtPostListTitle)
    TextView txtPostListTitle;
    private ServicePostListFragAdapter adapter;

    public ServicePostListFrag() {
    }

    public static ServicePostListFrag getInstance(String serviceTypeConsultancy) {
        serviceType = serviceTypeConsultancy;
        return new ServicePostListFrag();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //extract posts according to selected type from previous screen
        extractDataAccordingToServiceType();

    }

    /**
     * extract posts according to selected type from previous screen
     *
     * @see ServicesFragment
     */
    private void extractDataAccordingToServiceType() {
        if (ModelAllPosts.getInstance().getPosts() != null) {//posts have been downloaded
            for (ResPost post : ModelAllPosts.getInstance().getPosts()) {
                if (post.getType().equalsIgnoreCase(serviceType)) {
                    postList.add(post);

                    if (list != null) {
                        list.setVisibility(View.VISIBLE);
                        txtDataStatus.setVisibility(View.GONE);
                    }
                }
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_service_post_list, container, false);
        unbinder = ButterKnife.bind(this, view);

        //initialize the list
        setupList();

        return view;
    }

    /**
     * Initialize the list
     */
    private void setupList() {

        txtPostListTitle.setText(String.format("%s posts",serviceType));

        if (postList.size() > 0) {
            txtDataStatus.setVisibility(View.GONE);
            list.setVisibility(View.VISIBLE);
        }

        list.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new ServicePostListFragAdapter(getActivity(), postList);
        list.setAdapter(adapter);

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefresh.setRefreshing(false);
                downloadPostList();
            }
        });
    }

    /**
     * Download posts from server
     *
     * @see RemoteServices#getAllPosts()
     */
    private void downloadPostList() {
        Utils.showProgressDialog(getActivity());
        DataManager.getInstance().getAllPosts(new RemoteCallback<ModelAllPosts>() {
            @Override
            public void onSuccess(ModelAllPosts response) {
                Utils.hideProgressDialog();
                Timber.i("download posts list: " + new Gson().toJson(response));
                ModelAllPosts.setInstance(response);

                extractDataAccordingToServiceType();
            }

            @Override
            public void onFailed(Throwable throwable) {
                Utils.hideProgressDialog();
                Timber.e(throwable, "download posts list: ");
            }
        });
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

}
