package com.oss.oss.home;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oss.oss.R;
import com.oss.oss.Rest.DataManager;
import com.oss.oss.Rest.Resources.ResPost;
import com.oss.oss.Rest.model.ModelAllPosts;
import com.oss.oss.Rest.model.ModelUser;
import com.oss.oss.Rest.network.RemoteCallback;
import com.oss.oss.Rest.network.RemoteServices;
import com.oss.oss.about_austrailia.PostDialog;
import com.oss.oss.utils.Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import timber.log.Timber;

/**
 * Home Fragment - Main view of home activity extends {@link Fragment}
 *
 * @author Sapun Wiratunga <s.wiratunga@cqumail.com>
 * @version 1.0
 * @since 1.0
 */
public class HomeFragment extends Fragment {

    @BindView(R.id.list)
    RecyclerView list;
    Unbinder unbinder;
    ImageView imgHomeThumb;
    @BindView(R.id.txtDataStatus)
    TextView txtDataStatus;
    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefresh;
    List<ResPost> postsList = new ArrayList<>();
    @BindView(R.id.txtHomePost)
    TextView txtHomePost;
    private MultipartBody.Part postImage;
    private String title;
    private String text;
    private HomeFragmentAdapter adapter;

    /**
     * Constructor
     */
    public HomeFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //fetch post list from singleton and inflate in list
        if (ModelAllPosts.getInstance() != null) {//Post already downloaded at splash
            if (ModelAllPosts.getInstance().getPosts() != null) {
                postsList.addAll(ModelAllPosts.getInstance().getPosts());
            }
        }



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        unbinder = ButterKnife.bind(this, view);

        txtHomePost.setText(String.format("Hello %s %s write some posts here..", ModelUser.getInstance().getUser().getF_name(),ModelUser.getInstance().getUser().getL_name()));

        if (postsList.size() > 0) {
            list.setVisibility(View.VISIBLE);
            txtDataStatus.setVisibility(View.GONE);
        }
        list.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new HomeFragmentAdapter(getActivity(), postsList);
        list.setAdapter(adapter);

        //refresh list when dragged downwards
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefresh.setRefreshing(false);
                downloadPostList();
            }
        });
        return view;
    }


    /**
     * Download post list from server
     *
     * @see RemoteServices#getAllPosts()
     */
    private void downloadPostList() {
        Utils.showProgressDialog(getActivity());
        DataManager.getInstance().getAllPosts(new RemoteCallback<ModelAllPosts>() {
            @Override
            public void onSuccess(ModelAllPosts response) {
                Utils.hideProgressDialog();
                Timber.i("download posts list: " + new Gson().toJson(response));

                if (!response.isError()) {
                    if (response.getPosts() != null) {
                        list.setVisibility(View.VISIBLE);
                        txtDataStatus.setVisibility(View.GONE);

                        postsList.clear();
                        postsList.addAll(response.getPosts());
                        adapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailed(Throwable throwable) {
                Utils.hideProgressDialog();
                Timber.e(throwable, "download posts list: ");
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.txtHomePost)
    public void onViewClicked() {
        PostDialog postDialog=PostDialog.getInstance(new PostDialog.OnPostSentListener() {
            @Override
            public void onPostSent(ResPost message) {
                postsList.add(0, message);
                adapter.notifyItemInserted(0);
                list.smoothScrollToPosition(0);
            }
        });
        postDialog.show(getActivity().getSupportFragmentManager(),PostDialog.TAG);
    }




}
