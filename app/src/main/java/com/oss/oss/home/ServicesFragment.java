package com.oss.oss.home;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.oss.oss.R;
import com.oss.oss.Services.ServiceActivity;
import com.oss.oss.utils.FragmentManagerUtil;
import com.oss.oss.utils.TagsAndConstants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * ServicesFragment - Services main fragment
 * @author Sapun Wiratunga <s.wiratunga@cqumail.com>
 * @version 1.0
 * @since 1.0
 */
public class ServicesFragment extends Fragment {


    @BindView(R.id.btnServiceFragConsultancy)
    Button btnServiceFragConsultancy;
    @BindView(R.id.btnServiceFragCareer)
    Button btnServiceFragCareer;
    @BindView(R.id.btnServiceFragAccomodation)
    Button btnServiceFragAccomodation;
    Unbinder unbinder;

    /**
     * Constructor
     */
    public ServicesFragment() {
        // Required empty public constructor
    }


    /**
     * Constructor
     * @return
     * new {@link ServicesFragment} instance
     */
    public static Fragment getInstance() {
        return new ServicesFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_services, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.btnServiceFragConsultancy, R.id.btnServiceFragCareer, R.id.btnServiceFragAccomodation})
    public void onViewClicked(View view) {
        String serviceType=null;
        switch (view.getId()) {
            case R.id.btnServiceFragConsultancy:
                //show consultancy post list
                serviceType=TagsAndConstants.SERVICE_TYPE_CONSULTANCY;
                break;
            case R.id.btnServiceFragCareer:
                //show Career support post list
                serviceType=TagsAndConstants.SERVICE_TYPE_CAREER_SUPPORT;
                break;
            case R.id.btnServiceFragAccomodation:
                //show accomodation post list
                serviceType=TagsAndConstants.SERVICE_TYPE_ACCOMODATION;
                break;
        }

        Intent toServiceActivity=new Intent(getActivity(), ServiceActivity.class);
        toServiceActivity.putExtra(TagsAndConstants.EXTRA_SERVICE_TYPE,serviceType);
        startActivity(toServiceActivity);

        //show post list fragment
//        FragmentManagerUtil.addFragment(getActivity().getSupportFragmentManager(),R.id.frame_service,ServicePostListFrag.getInstance(serviceType),true,ServicePostListFrag.TAG);
    }
}
