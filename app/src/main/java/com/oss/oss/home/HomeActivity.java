package com.oss.oss.home;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.oss.oss.Login.UserActivity;
import com.oss.oss.R;
import com.oss.oss.Rest.model.ModelUser;
import com.oss.oss.about.AboutUsActivity;
import com.oss.oss.about_austrailia.AboutAustraliaActivity;
import com.oss.oss.admin.AdminActivity;
import com.oss.oss.admin.AdminFragment;
import com.oss.oss.contact.ContactUsActivity;
import com.oss.oss.contact.KeyContactActivity;
import com.oss.oss.utils.TagsAndConstants;
import com.oss.oss.utils.Utils;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Home activity - Home activity which holds navigation view and tabs for services
 *
 * @author Sapun Wiratunga <s.wiratunga@cqumail.com>
 * @version 1.0
 * @since 1.0
 */
public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private static OnImagePickedListner onImagePickedListener;
    @BindView(R.id.tabs)
    TabLayout tabs;
    @BindView(R.id.viewpager)
    ViewPager viewpager;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.btnNavDashboardLogout)
    Button btnNavDashboardLogout;
    @BindView(R.id.nav_view)
    NavigationView navView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    /**
     * List of icons to be used
     */
    private int[] tabIcons = {R.drawable.home, R.drawable.services, R.drawable.msg};
    private Uri postImageUri;
    private Bitmap postImageBitmap;
    private String postImagePath;

    /**
     * Register a callback to be invoked when image is picked from gallery.
     *
     * @param listener OnImagePickedListner instance
     * @see AdminFragment.OnDeleteUserClickListener
     */
    public static void setOnImagePickedListner(OnImagePickedListner listener) {
        onImagePickedListener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_main);

        //Butterknif injection for binding views
        ButterKnife.bind(this);

        //instantiate user singleton from preference
        initUser();

        //setup Toolbar and Tabbar
        setupAppbar();

        //setup navidation drawer
        setupNavigationView();

    }

    /**
     * Initialize {@link com.oss.oss.Rest.model.ModelUser} singleton
     */
    private void initUser() {
        //fetch user json data from preference
        String userJson = Utils.getSharedPreference(HomeActivity.this).getString(TagsAndConstants.TAG_USER_JSON, "");
        //instantiate user singleton
        ModelUser.setInstance(userJson);
    }

    /**
     * Setup Navigation Drawer
     */
    private void setupNavigationView() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();
        navView.setNavigationItemSelectedListener(this);
        navView.getMenu().getItem(0).setChecked(true);

        //set click listener for navigation view logout button
        navView.findViewById(R.id.btnNavDashboardLogout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //clear all preferences
                Utils.logoutUser(HomeActivity.this);
                startActivity(new Intent(HomeActivity.this, UserActivity.class));
                finish();
            }
        });

        //setup user data in navigation drawer
        TextView name = (TextView) navView.getHeaderView(0).findViewById(R.id.txtHeaderName);
        TextView email = (TextView) navView.getHeaderView(0).findViewById(R.id.txtHeaderEmail);
        name.setText(ModelUser.getInstance().getUser().getF_name() + " " + ModelUser.getInstance().getUser().getL_name());
        email.setText(ModelUser.getInstance().getUser().getEmail());

        //show special feature for admin
        if (ModelUser.getInstance().getUser().getUser_type().equalsIgnoreCase(TagsAndConstants.USER_TYPE_ADMIN)) {
            navView.getMenu().getItem(3).setVisible(true);//show admin panel menu
        } else {
            navView.getMenu().getItem(3).setVisible(false);//hide admin panel menu
        }
    }

    /**
     * Setup toolbar and Tabbar
     */
    private void setupAppbar() {
        setSupportActionBar(toolbar);
        setupViewPager(viewpager);
        tabs.setupWithViewPager(viewpager);
        setupTabIcons();
        tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                navView.getMenu().getItem(tab.getPosition()).setChecked(true);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    /**
     * Setup Services viewpager
     *
     * @param viewPager Services viewpager instance
     */
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new HomeFragment(), "Home");
        adapter.addFragment(ServicesFragment.getInstance(), "Services");
        adapter.addFragment(new ChatThreadsFragment(), "Message");
        viewPager.setAdapter(adapter);
    }

    /**
     * Attach icons with Services tabs
     */
    private void setupTabIcons() {
        tabs.getTabAt(0).setIcon(tabIcons[0]);
        tabs.getTabAt(1).setIcon(tabIcons[1]);
        tabs.getTabAt(2).setIcon(tabIcons[2]);
    }

    /**
     * setup image path from URI after picking from gallery
     *
     * @param selectedImageUri Image URI
     */
    private void setPostImagePath(Uri selectedImageUri) {
        String[] filePathColumn = {MediaStore.Images.Media.DATA};

        Cursor cursor = getContentResolver().query(selectedImageUri, filePathColumn, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            postImagePath = cursor.getString(columnIndex);

            cursor.close();


        } else {
            Toast.makeText(HomeActivity.this, "Cursor is null", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Create new file from provided bitmap
     *
     * @param photoBitmap Bitmap instance
     */
    public void createNewFile(Bitmap photoBitmap) {
        Bitmap photo = photoBitmap;
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/saved_images");
        myDir.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = "Image-" + n + ".jpg";
        File file = new File(myDir, fname);

        postImagePath = file.getAbsolutePath();
        if (file.exists()) file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            photo.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (onImagePickedListener != null) {
            onImagePickedListener.onImagePicked(postImagePath, postImageUri);

        }


    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        item.setChecked(true);
        switch (item.getItemId()) {
            case R.id.nav_home:
                tabs.getTabAt(0).select();
                break;
            case R.id.nav_services:
                tabs.getTabAt(1).select();
                break;
            case R.id.nav_messages:
                tabs.getTabAt(2).select();
                break;
            case R.id.nav_admin:
                startAdminActivity();
                break;

            case R.id.nav_about_austrailia:
                startActivity(new Intent(HomeActivity.this, AboutAustraliaActivity.class));
                break;

            case R.id.nav_keyConntacts:
                startActivity(new Intent(HomeActivity.this, KeyContactActivity.class));
                break;

            case R.id.nav_contactus:
                startActivity(new Intent(HomeActivity.this, ContactUsActivity.class));
                break;
            case R.id.nav_aboutus:
                startActivity(new Intent(HomeActivity.this, AboutUsActivity.class));
                break;
        }
        drawerLayout.closeDrawer(GravityCompat.START);
        return false;
    }

    /**
     * start to {@link AdminActivity}
     */
    private void startAdminActivity() {
        startActivity(new Intent(HomeActivity.this, AdminActivity.class));
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //pick image from gallery
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == AppCompatActivity.RESULT_OK) {
            postImageUri = CropImage.getPickImageResultUri(this, data);

            try {
                postImageBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), postImageUri);
            } catch (IOException e) {
                e.printStackTrace();
            }

            //setup picked image path
            setPostImagePath(postImageUri);

            //start crop activity for picked image
            CropImage.activity(postImageUri)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .start(HomeActivity.this);
        }

        //pick image from crop activity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                postImageUri = result.getUri();
                createNewFile(postImageBitmap);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    /**
     * Interface definition for a callback to be invoked when image is picked.
     */
    public interface OnImagePickedListner {
        public void onImagePicked(String postImagePath, Uri postImageUri);
    }

    /**
     * View pager Adapter extends {@link FragmentPagerAdapter}
     */
    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }
    }
}
