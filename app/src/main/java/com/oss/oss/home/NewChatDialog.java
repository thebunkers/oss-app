package com.oss.oss.home;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oss.oss.R;
import com.oss.oss.Rest.DataManager;
import com.oss.oss.Rest.Resources.ResUser;
import com.oss.oss.Rest.model.ModelAllUsers;
import com.oss.oss.Rest.model.ModelUser;
import com.oss.oss.Rest.network.RemoteCallback;
import com.oss.oss.Rest.network.RemoteServices;
import com.oss.oss.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import timber.log.Timber;

/**
 * NewChatDialog - Dialog to create new chat
 *
 * @author Sapun Wiratunga <s.wiratunga@cqumail.com>
 * @version 1.0
 * @since 1.0
 */
public class NewChatDialog extends DialogFragment {


    public static final String TAG = NewChatDialog.class.getSimpleName();
    Unbinder unbinder;
    @BindView(R.id.txtDataStatus)
    TextView txtDataStatus;
    @BindView(R.id.list)
    RecyclerView list;
    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefresh;
    private List<ResUser> userList=new ArrayList<>();
    private NewChatDialogAdapter adapter;

    /**
     * default Constructor
     */
    public NewChatDialog() {
        // Required empty public constructor
    }

    /**
     * Parameterized Constructor
     *
     * @return new NewChatDialog instance
     */
    public static NewChatDialog getInstance() {
        return new NewChatDialog();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.getDialog().getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        View view = inflater.inflate(R.layout.dialog_new_chat, container, false);
        //Butterknif injection for binding view
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //setup list
        setupList();

        //download users
        Utils.showProgressDialog(getActivity());
        downloadUsers();

    }

    private void setupList() {
        list.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new NewChatDialogAdapter(getActivity(), userList);
        list.setAdapter(adapter);

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                downloadUsers();
            }
        });
    }

    /**
     * Doanlod list of users
     * @see RemoteServices#getAllUsers(String)
     */
    private void downloadUsers() {
        DataManager.getInstance().getAllUsers(ModelUser.getInstance().getUser().getUser_id(), new RemoteCallback<ModelAllUsers>() {
            @Override
            public void onSuccess(ModelAllUsers response) {
                swipeRefresh.setRefreshing(false);
                Utils.hideProgressDialog();
                Timber.i("downloadUsersList: "+new Gson().toJson(response));

                if(!response.isError()){
                    list.setVisibility(View.VISIBLE);
                    txtDataStatus.setVisibility(View.GONE);

                    userList.clear();
                    userList.addAll(response.getUsers());
                    adapter.notifyDataSetChanged();
                }

            }

            @Override
            public void onFailed(Throwable throwable) {
                swipeRefresh.setRefreshing(false);
                Utils.hideProgressDialog();
                Timber.e(throwable,"downloadUsersList: ");
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getDialog() == null)
            return;
        getDialog().getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

}
