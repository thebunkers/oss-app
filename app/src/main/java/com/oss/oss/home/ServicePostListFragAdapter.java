package com.oss.oss.home;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oss.oss.R;
import com.oss.oss.Rest.DataManager;
import com.oss.oss.Rest.Resources.ResPost;
import com.oss.oss.Rest.model.ModelDeletePost;
import com.oss.oss.Rest.model.ModelUser;
import com.oss.oss.Rest.network.RemoteCallback;
import com.oss.oss.utils.DialogUtils;
import com.oss.oss.utils.FragmentManagerUtil;
import com.oss.oss.utils.TagsAndConstants;
import com.oss.oss.utils.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

/**
 * ServicePostListFragAdapter - Adapter for {@link ServicePostListFrag}
 *
 * @author Sapun Wiratunga <s.wiratunga@cqumail.com>
 * @version 1.0
 * @since 1.0
 */
public class ServicePostListFragAdapter extends RecyclerView.Adapter<ServicePostListFragAdapter.ViewHolder> {

    private final List<ResPost> mValues;
    private Context context;

    /**
     * Constructor
     *
     * @param ctx   View's context
     * @param items
     */
    public ServicePostListFragAdapter(Context ctx, List<ResPost> items) {
        context = ctx;
        mValues = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_service_post_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);

        //Bind post item with view holder
        ((ViewHolder) holder).bindData();

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    /**
     * ViewHolder for Post item
     *
     * @see android.support.v7.widget.RecyclerView.ViewHolder
     */
    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public ResPost mItem;
        @BindView(R.id.txtItemServicePostTitle)
        TextView txtItemServicePostTitle;
        @BindView(R.id.txtItemServicePostTime)
        TextView txtItemServicePostTime;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            ButterKnife.bind(this, view);

            mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FragmentManagerUtil.replaceFragment(((AppCompatActivity) context).getSupportFragmentManager(), R.id.frameServices, ServicePostDetail.getInstance(mItem), true, ServicePostDetail.TAG);
                }
            });

            mView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (ModelUser.getInstance().getUser().getUser_type().equalsIgnoreCase(TagsAndConstants.USER_TYPE_ADMIN)) {

                        //delete post
                        deletePost();
                    }
                    return true;
                }
            });
        }

        /**
         * Delete post
         *
         * @see com.oss.oss.Rest.network.RemoteServices#deletePost(String)
         */
        private void deletePost() {

            DialogUtils.showDialog(context, "Delete post", "Are you sure you want to delete this post", "Delete post", "Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Utils.showProgressDialog(context);
                    DataManager.getInstance().deletePost(mItem.getPost_id(), new RemoteCallback<ModelDeletePost>() {
                        @Override
                        public void onSuccess(ModelDeletePost response) {
                            Utils.hideProgressDialog();
                            Timber.i("DeletePost: " + new Gson().toJson(response));

                            if (!response.isError()) {//post deleted successfully
                                mValues.remove(getAdapterPosition());
                                ServicePostListFragAdapter.this.notifyItemRemoved(getAdapterPosition());
                            }
                        }

                        @Override
                        public void onFailed(Throwable throwable) {
                            Utils.hideProgressDialog();
                            Timber.e(throwable, "DeletePost: ");
                        }
                    });
                }
            });
        }


        //Bind post item with current viewholder
        public void bindData() {
            txtItemServicePostTitle.setText(mItem.getTitle());
            txtItemServicePostTime.setText(Utils.calculateTimeAgo(Utils.getDateInMillis(mItem.getCreated_on()), System.currentTimeMillis()));
        }
    }
}
