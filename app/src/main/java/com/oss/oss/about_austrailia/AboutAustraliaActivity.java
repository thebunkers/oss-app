package com.oss.oss.about_austrailia;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.oss.oss.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AboutAustraliaActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.wvAboutAustrailia)
    WebView wvAboutAustrailia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_austrailia);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        WebSettings webSettings = wvAboutAustrailia.getSettings();
        webSettings.setJavaScriptEnabled(true);
        wvAboutAustrailia.loadUrl("http://www.about-australia.com/");
//        wvAboutAustrailia.loadUrl("file:///android_asset/test.html");
        wvAboutAustrailia.setWebViewClient(new MyAppWebViewClient());

        wvAboutAustrailia.getSettings().setLoadWithOverviewMode(true);
        wvAboutAustrailia.getSettings().setUseWideViewPort(true);

    }

    @Override
    public void onBackPressed() {
        if(wvAboutAustrailia.canGoBack()) {
            wvAboutAustrailia.goBack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            default:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
