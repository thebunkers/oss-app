package com.oss.oss.about_austrailia;


import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.google.gson.Gson;
import com.oss.oss.R;
import com.oss.oss.Rest.DataManager;
import com.oss.oss.Rest.Resources.ResMsg;
import com.oss.oss.Rest.Resources.ResPost;
import com.oss.oss.Rest.model.ModelAllPosts;
import com.oss.oss.Rest.model.ModelNewPost;
import com.oss.oss.Rest.model.ModelUser;
import com.oss.oss.Rest.network.RemoteCallback;
import com.oss.oss.Rest.network.RemoteServices;
import com.oss.oss.home.HomeActivity;
import com.oss.oss.utils.Utils;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import timber.log.Timber;

/**
 * PostDialog - Dialog to create new posts
 *
 * @author Sapun Wiratunga <s.wiratunga@cqumail.com>
 * @version 1.0
 * @since 1.0
 */
public class PostDialog extends DialogFragment {


    public static final String TAG = PostDialog.class.getSimpleName();
    private static OnPostSentListener onPostSentListener;
    Unbinder unbinder;
    @BindView(R.id.spiHomePostType)
    Spinner spiHomePostType;
    @BindView(R.id.edtHomePostTitle)
    EditText edtHomePostTitle;
    @BindView(R.id.edtHomePostText)
    EditText edtHomePostText;
    @BindView(R.id.btnHomePost)
    Button btnHomePost;
    @BindView(R.id.btnHomePickImage)
    ImageButton btnHomePickImage;
    @BindView(R.id.imgHomeThumb)
    ImageView imgHomeThumb;
    @BindView(R.id.card_view)
    CardView cardView;
    private String title;
    private String text;
    private MultipartBody.Part postImage;

    /**
     * default Constructor
     */
    public PostDialog() {
        // Required empty public constructor
    }

    /**
     * Parameterized Constructor
     *
     * @param listener OnUserSelectedListener instance
     * @return new NewChatDialog instance
     */
    public static PostDialog getInstance(OnPostSentListener listener) {
        onPostSentListener = listener;
        return new PostDialog();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.getDialog().getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        View view = inflater.inflate(R.layout.fragment_post_dialog, container, false);
        //Butterknif injection for binding view
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //register for image picked listner
        HomeActivity.setOnImagePickedListner(new HomeActivity.OnImagePickedListner() {
            @Override
            public void onImagePicked(String postImagePath, Uri postImageUri) {

                imgHomeThumb.setVisibility(View.VISIBLE);
                imgHomeThumb.setImageURI(postImageUri);

                if (postImagePath != null) {
                    //File creating from selected URL
                    File file = new File(postImagePath);
                    // create RequestBody instance from file
                    RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                    // MultipartBody.Part is used to send also the actual file name
                    postImage =
                            MultipartBody.Part.createFormData("image", file.getName(), requestFile);
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getDialog() == null)
            return;
        getDialog().getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    /**
     * Send new message to server
     *
     * @see RemoteServices#sendMsg(String, String)
     */
    private void sendNewPost() {
        Utils.showProgressDialog(getActivity());

    }

    public static interface OnPostSentListener {
        public void onPostSent(ResPost post);
    }


    @OnClick({R.id.btnHomePost, R.id.btnHomePickImage})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnHomePost:
                btnHomePost.setEnabled(false);
                if (validation()) {
                    RequestBody postTitleRequestBody = RequestBody.create(MediaType.parse("text/plain"), title);
                    RequestBody postTextRequestBody = RequestBody.create(MediaType.parse("text/plain"), text);
                    RequestBody postTypeRequestBody = RequestBody.create(MediaType.parse("text/plain"), spiHomePostType.getSelectedItem().toString().trim().toLowerCase());
                    RequestBody postCreatedByRequestBody = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(ModelUser.getInstance().getUser().getUser_id()));

                    Utils.showProgressDialog(getActivity());
                    DataManager.getInstance().sendPost(postTypeRequestBody, postCreatedByRequestBody, postTitleRequestBody, postTextRequestBody, postImage, new RemoteCallback<ModelNewPost>() {
                        @Override
                        public void onSuccess(ModelNewPost response) {
                            Utils.hideProgressDialog();
                            Timber.i("sendPost : " + new Gson().toJson(response));

                            if (!response.isError()) {//post sent successfully
                                imgHomeThumb.setVisibility(View.GONE);
                                edtHomePostText.setText(null);
                                edtHomePostTitle.setText(null);
                                btnHomePost.setEnabled(true);
                            }

                            //add newly added post to post model singelton
                            ModelAllPosts.getInstance().getPosts().add(response.getPost());
//                            //add newly added post to recent post list
//                            postsList.add(response.getPost());
//                            //notify recent post list adapter
//                            adapter.notifyDataSetChanged();
                            onPostSentListener.onPostSent(response.getPost());
                            PostDialog.this.dismiss();
                        }

                        @Override
                        public void onFailed(Throwable throwable) {
                            Utils.hideProgressDialog();
                            Timber.e(throwable, "sendPost :");
                            btnHomePost.setEnabled(true);
                        }
                    });

                }
                break;
            case R.id.btnHomePickImage:
                CropImage.startPickImageActivity(getActivity());
                break;
        }
    }

    private boolean validation() {
        boolean validation = true;

        title = edtHomePostTitle.getText().toString().trim();
        text = edtHomePostText.getText().toString().trim();

        if (TextUtils.isEmpty(title)) {
            edtHomePostTitle.setError(getResources().getString(R.string.error_title));
            validation = false;
        }

        if (TextUtils.isEmpty(text)) {
            edtHomePostText.setError(getResources().getString(R.string.error_post_text));
            validation = false;
        }
        return validation;
    }
}
