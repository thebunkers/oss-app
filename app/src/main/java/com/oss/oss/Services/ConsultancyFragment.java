package com.oss.oss.Services;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

import com.oss.oss.R;
import com.oss.oss.about_austrailia.MyAppWebViewClient;
import com.oss.oss.home.ServicePostListFrag;
import com.oss.oss.utils.FragmentManagerUtil;
import com.oss.oss.utils.TagsAndConstants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class ConsultancyFragment extends Fragment {


    public static final String TAG = ConsultancyFragment.class.getSimpleName();
    @BindView(R.id.btnConsultancyViewAll)
    Button btnConsultancyViewAll;
    Unbinder unbinder;
    @BindView(R.id.webview)
    WebView webview;

    public ConsultancyFragment() {
        // Required empty public constructor
    }


    public static Fragment getInstance() {
        return new ConsultancyFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_consultancy, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        WebSettings webSettings = webview.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webview.loadUrl("file:///android_asset/consultancy_service.html");
        webview.setWebViewClient(new MyAppWebViewClient());
        webview.getSettings().setLoadWithOverviewMode(true);
        webview.getSettings().setUseWideViewPort(true);

        webview.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_BACK:
                            if (webview.canGoBack()) {
                                webview.goBack();
                            } else {
                                getActivity().onBackPressed();
                            }
                            return true;
                    }

                }
                return false;
            }
        });
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.btnConsultancyViewAll)
    public void onViewClicked() {
        FragmentManagerUtil.replaceFragment(getActivity().getSupportFragmentManager(), R.id.frameServices, ServicePostListFrag.getInstance(TagsAndConstants.SERVICE_TYPE_CONSULTANCY), true, ServicePostListFrag.TAG);
    }
}
