package com.oss.oss.Services;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.oss.oss.R;
import com.oss.oss.Rest.Resources.ResPost;
import com.oss.oss.home.ServicePostDetail;
import com.oss.oss.home.ServicePostListFrag;
import com.oss.oss.utils.FragmentManagerUtil;
import com.oss.oss.utils.TagsAndConstants;

public class ServiceActivity extends AppCompatActivity {

    private static ResPost postItem=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if(getIntent().getExtras()!=null){
            Fragment targetFragment=null;
            String tag=null;
            switch (getIntent().getExtras().getString(TagsAndConstants.EXTRA_SERVICE_TYPE)){
                case TagsAndConstants.SERVICE_TYPE_CONSULTANCY:
                    targetFragment=ConsultancyFragment.getInstance();
                    tag=ConsultancyFragment.TAG;
                    break;
                case TagsAndConstants.SERVICE_TYPE_CAREER_SUPPORT:
                    targetFragment=CareerSupportFragment.getInstance();
                    tag=CareerSupportFragment.TAG;
                    break;
                case TagsAndConstants.SERVICE_TYPE_ACCOMODATION:
                    targetFragment=AccomodationFragment.getInstance();
                    tag=AccomodationFragment.TAG;
                    break;
            }
            FragmentManagerUtil.replaceFragment(getSupportFragmentManager(),R.id.frameServices, targetFragment,false,tag);
        }

        //if post item is not null that means to show detail activity
        if(postItem!=null){
            FragmentManagerUtil.replaceFragment(getSupportFragmentManager(),R.id.frameServices, ServicePostDetail.getInstance(postItem),false,ServicePostDetail.TAG);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            default:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    public static void startPostDetailWith(Context context, ResPost mItem) {

        postItem = mItem;
        context.startActivity(new Intent(context,ServiceActivity.class));
    }

    @Override
    protected void onDestroy() {
        postItem=null;
        super.onDestroy();

    }
}
