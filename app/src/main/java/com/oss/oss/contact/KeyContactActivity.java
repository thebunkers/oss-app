package com.oss.oss.contact;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oss.oss.R;
import com.oss.oss.Rest.Resources.ResKeyContact;
import com.oss.oss.home.HomeFragmentAdapter;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class KeyContactActivity extends AppCompatActivity {

    @BindView(R.id.txtDataStatus)
    TextView txtDataStatus;
    @BindView(R.id.list)
    RecyclerView list;
    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefresh;

    List<ResKeyContact.DataBean> keyContactList = new ArrayList<>();
    private KeyContactsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_key_contact);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setupList();

        fetchData();


    }

    private void setupList() {
        list.setLayoutManager(new LinearLayoutManager(this));
        adapter = new KeyContactsAdapter(KeyContactActivity.this, keyContactList);
        list.setAdapter(adapter);

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchData();
            }
        });
    }

    private void fetchData() {
        swipeRefresh.setRefreshing(false);
        ResKeyContact resKeyContact = new Gson().fromJson(loadJSONFromAsset(), ResKeyContact.class);
        if(resKeyContact.getData().size()>0){
            keyContactList.clear();
            list.setVisibility(View.VISIBLE);
            txtDataStatus.setVisibility(View.GONE);
            for(ResKeyContact.DataBean contact:resKeyContact.getData()){
                keyContactList.add(contact);
            }
            adapter.notifyDataSetChanged();
        }
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getAssets().open("key_contacts.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            default:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
