package com.oss.oss.contact;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oss.oss.R;
import com.oss.oss.Rest.Resources.ResKeyContact;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * HomeFragmentAdapter - Main view of home activity extends {@link RecyclerView.Adapter}
 *
 * @author Sapun Wiratunga <s.wiratunga@cqumail.com>
 * @version 1.0
 * @since 1.0
 */
public class KeyContactsAdapter extends RecyclerView.Adapter<KeyContactsAdapter.ViewHolder> {

    private final List<ResKeyContact.DataBean> mValues;
    private Context context;

    /**
     * Constructor
     *
     * @param ctx   Context from where it gets invoked
     * @param items list of Posts
     */
    public KeyContactsAdapter(Context ctx, List<ResKeyContact.DataBean> items) {
        context = ctx;
        mValues = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_key_contacts, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        //bind post item with viewholder
        ((ViewHolder) holder).bindData();

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    /**
     * View holder for post items extends {@link ViewHolder}
     */
    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public ResKeyContact.DataBean mItem;
        @BindView(R.id.txtItemKeyContactName)
        TextView txtItemKeyContactName;
        @BindView(R.id.txtItemKeyContactWhenToUse)
        TextView txtItemKeyContactWhenToUse;
        @BindView(R.id.txtItemKeyContactContact)
        TextView txtItemKeyContactContact;

        public ViewHolder(View view) {
            super(view);
            mView = view;

            //Butterknif injection
            ButterKnife.bind(this, view);

        }

        public void bindData() {
            txtItemKeyContactName.setText(mItem.getName());
            txtItemKeyContactWhenToUse.setText(mItem.getWhen_to_use());
            txtItemKeyContactContact.setText(mItem.getContact());
        }
    }
}
